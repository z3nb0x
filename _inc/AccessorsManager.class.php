<?php

abstract class AccessorsManager
{

	// METHODS

	public function __call($method, $attrs)
	{
		$prefix  = substr($method, 0, 3);

		$suffix  = self::lcfirst(substr($method, 3, 1));
		$suffix .= substr($method, 4);

		$cattrs  = count($attrs);

		if(property_exists($this, $suffix))
		{
			if($prefix=='set')
			{
				if($cattrs == 1)
					return $this->set($suffix, $attrs[0]);
				else
					trigger_error(__CLASS__." : Calling method <i>$method</i> failed : you have to specify a value !", E_USER_ERROR);
			}

			if($prefix=='get' && $cattrs==0)
			{
				return $this->get($suffix);
			}
		}
		else
		{
			trigger_error(__CLASS__." : Calling method <i>$method</i> failed : member <i>$suffix</i> doesn't exist !", E_USER_ERROR);
		}
	}



	public function set($key, $val)
	{
		$this->$key = $val;
	}



	public function get($key)
	{
		return $this->$key;
	}



	private static function lcfirst($str)
	{
		$firstletter = substr($str,0,1);
		$fl = chr(ord($firstletter) + 32);

		$res = $fl.substr($str,1,strlen($str)-1);

		return $res;
	}
}
?>