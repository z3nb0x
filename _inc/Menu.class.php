<?php
	require_once(dirname(__FILE__)."/AccessorsManager.class.php");

	class Menu extends AccessorsManager
	{
		// MEMBERS

		const CLASS_VERSION = "20080407";

		protected $menu;
		protected $file;





		// CONSTRUCTOR

		public function __construct($file)
		{
			$this->setFile($file);
			$this->setMenu(parse_ini_file($file,true));
		}





		// GETTERS & SETTERS
		/*
		 * Getters and Setters are automatically build by the AccessorsManager Class.
		 *
		 * You don't need to write them unless you want them to do specific things.
		 *
		 */





		// METHODS

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}

		public function buildMenu($key="MAIN")
		{
			$res = false;
			$menu = $this->getMenu();

			if(array_key_exists($key,$menu))
			{
				$res = $menu[$key];
			}
			else
			{
				trigger_error(__METHOD__." : The given key (<i>".$key."</i>) does not exist !", E_USER_ERROR);
			}

			return $res;
		}

		public function getParentLink($key)
		{
			$res = false;
			$menu = $this->getMenu();

			foreach($menu as $m)
			{
				$k = key($menu);
				foreach($m as $m2)
				{
					if($m2==$key)
						$res = $k;
				}
				next($menu);
			}

			if($res === false)
			{
				trigger_error(__METHOD__." : The given key (<i>".$key."</i>) does not exist !", E_USER_ERROR);
			}

			return $res;
		}
	}
?>