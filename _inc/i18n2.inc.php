<?php
	putenv("LANG=".$module->getI18n());
	setlocale(LC_MESSAGES, $module->getI18n());

	bindtextdomain("z3nb0x", dirname(__FILE__)."/../_i18n/");
	bind_textdomain_codeset("z3nb0x", "UTF-8");
?>