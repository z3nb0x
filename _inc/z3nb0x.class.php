<?php
	require_once(dirname(__FILE__)."/AccessorsManager.class.php");
	require_once(dirname(__FILE__)."/z3nErrorManager.class.php");

	class z3nb0x extends AccessorsManager
	{
		// ATTRIBUTES :

		const CLASS_VERSION = "20080430";			//

		protected $installPath;					//
		protected $url;						//
		protected $i18n;					//
		protected $theme;					//
		protected $mediaPath;					//
		protected $cachePath;					//
		protected $errorManager;				// Object	: instance of z3nErrorManager.



		// CONSTRUCTOR :

		public function __construct()
		{
			// Read Z3NB0X configuration from Z3NB0X.ini file :
			$iniFile = dirname(__FILE__)."/Z3NB0X.ini";

			if(@fopen($iniFile,"r") !== false)
			{
				$ini = parse_ini_file($iniFile);

				$this->setInstallPath($ini["INSTALL_PATH"]);
				$this->setUrl($ini["URL"]);
				$this->setI18n($ini["I18N"]);
				$this->setTheme($ini["THEME"]);
				$this->setMediaPath($ini["MEDIA_PATH"]);
				$this->setCachePath($ini["CACHE_PATH"]);

				// Setup the error manager :
				$this->setErrorManager(z3nErrorManager::singleton());
			}
			else
			{
				trigger_error(sprintf(gettext("z3nb0x's configuration file (<i>%s</i>) is missing or not readable !"), $iniFile), E_USER_ERROR);
			}
		}



		// PUBLIC METHODS :

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}


		public function display()
		{
			ob_start();

			if($this->getErrorManager()->hasError())
			{
				$errs = $this->getErrorManager()->getErrors();
				$err = $errs[0];
				include(dirname(__FILE__)."/../error_z3nb0x.tpl.php");
			}
			else
			{
				include(dirname(__FILE__)."/../index.tpl.php");
			}

			$page = ob_get_contents();
        		ob_end_clean();

        		echo $page;
		}
	}
?>