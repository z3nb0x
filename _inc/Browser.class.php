<?php
	require_once(dirname(__FILE__)."/AccessorsManager.class.php");

	class Browser extends AccessorsManager
	{
		// MEMBERS

		const CLASS_VERSION = "20080403";
		const SORT_ASC = 0;
		const SORT_DESC = 1;

		protected $path;
		protected $dirs;
		protected $files;





		// CONSTRUCTOR

		public function __construct($path)
		{
			$this->setPath($path);
			$dirs = array();
			$files = array();

			if(is_dir($this->getPath()))
			{
				if($folder = opendir($this->getPath()))
				{
					while(false !== ($file = readdir($folder)))
					{
						if(is_dir($this->getPath()."/".$file))
						{
							$dirs[] = $file;
						}
						else
						{
							$files[] = $file;
						}
					}

					$this->setDirs($dirs);
					$this->setFiles($files);

					@closedir($folder);
				}
				else
				{
					throw new Exception("Unable to open the folder <i>".$this->getPath()."</i>.");
				}
			}
			else
			{
				throw new Exception("The given path <i>".$this->getPath()."</i> doesn't seem to be an existing directory.");
			}
		}





		// GETTERS & SETTERS
		/*
		 * Getters and Setters are automatically build by the AccessorsManager Class.
		 *
		 * You don't need to write them unless you want them to do specific things.
		 *
		 */




		// METHODS

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}



		public function order_dirs($order=self::SORT_ASC)
		{
			$res = false;

			$arr = $this->getDirs();
			$res1 = natcasesort($arr);

			$this->setDirs($arr);

			if($order==self::SORT_DESC && $res1)
			{
				$this->setDirs(array_reverse($this->getDirs()));
			}

			if($res1)
				$res = !$res;

			return $res;
		}



		public function order_files($order=self::SORT_ASC)
		{
			$res = false;

			$arr = $this->getFiles();
			$res1 = natcasesort($arr);

			$this->setFiles($arr);

			if($order==self::SORT_DESC && $res1)
			{
				$this->setFiles(array_reverse($this->getFiles()));
			}

			if($res1)
				$res = !$res;

			return $res;
		}



		public function getList()
		{
			$res = array_merge($this->getDirs(),$this->getFiles());

			return $res;
		}



		public function filter($auth_ext)
		{
			$res = array();
			$files = $this->getFiles();

			foreach($files as $f)
			{
				$ext = self::get_extension($f);

				if(in_array($ext, $auth_ext))
				{
					$res[] = $f;
				}
			}

			$this->setFiles($res);
		}



		public function hide_hidden_dirs()
		{
			$dirs = array();

			foreach($this->getDirs() as $dir)
			{
				if(substr($dir,0,1)!=".")
				{
					$dirs[] = $dir;
				}
			}

			$this->setDirs($dirs);
		}



		public function hide_hidden_files()
		{
			$files = array();

			foreach($this->getFiles() as $file)
			{
				if(substr($file,0,1)!=".")
				{
					$files[] = $file;
				}
			}

			$this->setFiles($files);
		}



		public function hide_all()
		{
			$this->hide_hidden_dirs();
			$this->hide_hidden_files();
		}



		public function delete_dir($dir)
		{
			$res = array();

			foreach($this->getDirs() as $d)
			{
				if($d!=$dir)
					$res[] = $d;
			}

			$this->setDirs($res);
		}



		public static function get_extension($f)
		{
			$res = "unknown";

   			$parts = explode(".",$f);
   			$size = count($parts);

			if($size!=0)
			{
				$res = $parts[$size-1];
   			}

			return $res;
		}
	}
?>