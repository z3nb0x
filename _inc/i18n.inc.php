<?php
	putenv("LANG=".$_Z3N__LANG_I18N);
	setlocale(LC_MESSAGES, $_Z3N__LANG_I18N);

	bindtextdomain("z3nb0x",dirname(__FILE__)."/../_i18n/");
	bind_textdomain_codeset("z3nb0x","UTF-8");
?>