<?php
	require_once(dirname(__FILE__)."/z3nb0x.class.php");

	abstract class Module extends z3nb0x
	{
		// MEMBERS

		const CLASS_VERSION = "20080523";	// Version of this file (must be a yyyymmdd formated date).

		// These parameters are set from the MODULE.xml file :
		protected $description;			// String	: Description of module.
		protected $copyright;			// String	: Copyright.
		protected $changelog;			// Array	: Changelog entries.

		// Those ones are always built here :
		protected $moduleConfig;		// Array	: Configuration of module, read from the module's MODULE.ini file.
		protected $template;			// String	: Path to the file used to display the page.
		protected $picture;			// String	: Path to the module's picture.

		// Those ones will be set by the extended class :
		protected $name;			// String	: Name of plugin.
		protected $backlink;			// String 	: URL of the page where you want to go when you push the back button.






		// CONSTRUCTOR

		public function __construct($tpl_file)
		{
			parent::__construct();

			// Read MODULE configuration from MODULE.ini file :
			if(@fopen("MODULE.ini","r") !== false)
			{
				$this->setModuleConfig(parse_ini_file("MODULE.ini"));
			}
			else
			{
				$msg = gettext("Module configuration error !");
				$exp = gettext("I wasn't able to read the required <i>MODULE.ini</i> configuration file !");

				$this->getErrorManager()->addError(new z3nError($msg, $exp));
			}


			// Sets the template file :
			if(@fopen($tpl_file, "r"))
			{
					$this->setTemplate($tpl_file);
			}
			else
			{
				$msg = gettext("Module parameter error !");
				$exp = sprintf(gettext("I wasn't able to read the given template file (<i>%s</i>) !"), htmlentities($tpl_file));

				$this->getErrorManager()->addError(new z3nError($msg, $exp));
			}


			// Sets the module picture :
			if(@fopen("MODULE.png","r"))
				$this->setPicture("MODULE.png");	// We use the MODULE.png file.
			else
				$this->setPicture("");			// Falls back to nothing.
		}





		// GETTERS & SETTERS
		/*
		 * Getters and Setters are automatically build by the AccessorsManager Class.
		 *
		 * You don't need to write them unless you want them to do specific things.
		 *
		 */

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}



		// PUBLIC METHODS

		public function is_this_a_new_version($v)
		{
			$res = version_compare($this->getVersion(),$v,">");

			return $res;
		}


		public function display()
		{
			ob_start();

			if($this->getErrorManager()->hasError())
			{
				$errs = $this->getErrorManager()->getErrors();
				$err = $errs[0];
				include(dirname(__FILE__)."/../error_module.tpl.php");
			}
			else
			{
				include($this->getTemplate());
			}

			$page = ob_get_contents();
        		ob_end_clean();

        		echo $page;
		}



		// PUBLIC ABSTRACT METHODS

		abstract public function buildModule();



		// PUBLIC STATIC METHODS

		public static function readXMLDoc($f)
		{
			$ret = NULL;

			if(file_exists($f))
			{
				$sXMLElemObj = simplexml_load_file($f);
				$ret["name"] = (string)$sXMLElemObj->name;
				$ret["description"] = (string)$sXMLElemObj->description;
				$ret["maintainer"]["name"] = (string)$sXMLElemObj->maintainer;
				$ret["maintainer"]["email"] = (string)$sXMLElemObj->maintainer_email;
			}
			else
			{
				trigger_error(__METHOD__." : Specified file does not exist !");
			}

			return $ret;
		}
	}
?>