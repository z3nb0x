<?php
	require_once(dirname(__FILE__)."/AccessorsManager.class.php");


	class Image extends AccessorsManager
	{
		// MEMBRES

		const CLASS_VERSION = "20080407";

		protected $img;
		protected $width;
		protected $height;





		// CONSTRUCTOR

		public function __construct($file)
		{
			$size = getimagesize($file);

			$this->setWidth($size[0]);
			$this->setHeight($size[1]);
			$this->setImg(self::create_image_from($file,$size[2]));
		}





		// DESTRUCTOR

		public function __destruct()
		{
			//todo
		}





		// GETTERS & SETTERS
		/*
		 * Getters and Setters are automatically build by the AccessorsManager Class.
		 *
		 * You don't need to write them unless you want them to do specific things.
		 *
		 */

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}





		// METHODS

		// PUBLIC METHODS

		public function resize($w,$h)
		{
			$size[0] = $this->getWidth();
			$size[1] = $this->getHeight();

			$scale = min($w/$size[0], $h/$size[1]);
			$new_width = (int)($size[0]*$scale);
			$new_height = (int)($size[1]*$scale);

			$dst_img = imagecreatetruecolor($new_width, $new_height);

			imagecopyresampled($dst_img, $this->getImg(), 0, 0, 0, 0, $new_width, $new_height, $this->getWidth(), $this->getHeight());

			$this->setImg($dst_img);
			$this->setWidth($new_width);
			$this->setHeight($new_height);
		}

		public function create_reflection($opacity, $reflection_height_percent, $bgcolor="000000")
		{
			// We get useful information from the original picture :
			$src_img = $this->getImg();
			$w = $this->getWidth();
			$h = $this->getHeight();

			// We have to know some things about the reflection effect :
			$reflection_height = intval($h*($reflection_height_percent/100));

			$nh = $h+$reflection_height;

			// Creation of the new picture :
			$dst_img = imagecreatetruecolor($w,$nh);
			$this->setImg($dst_img);
			$this->setHeight($nh);

			// Then, we have to copy the part of pic that will be reflected :
			imagecopy($this->getImg(), $src_img, 0, 0, 0, ($h-$reflection_height), $w, $reflection_height);

			// And flip it vertically :
			$this->flip("VERTICAL");

			// Then we have to copy the original pic :
			imagecopy($this->getImg(), $src_img, 0, 0, 0, 0, $w, $h);

			// And finally, we draw the alpha gradient :
			$colors = self::hex2rgb($bgcolor);

			for($i=0 ; $i<$reflection_height ; $i++)
			{
				$alpha = imagecolorallocatealpha($this->getImg(), $colors["r"], $colors["v"], $colors["b"], ($i/$reflection_height*-1+1)*$opacity);
				imagefilledrectangle($this->getImg(), 0, $h+$i, $w, $h+$i, $alpha);
			}
		}

		public function flip($p)
		{
			$res = NULL;

			if($p=="HORIZONTAL")
				$res = $this->flip_h();
			else if($p=="VERTICAL")
				$res = $this->flip_v();

			return $res;
		}

		public function draw($type)
		{
			switch($type)
			{
				case "JPG" :
					header("Content-type: image/jpeg");
					$res = imagejpeg($this->getImg());
					break;

				case "PNG" :
					header("Content-type: image/png");
					$res = imagepng($this->getImg());
					break;

				case "GIF" :
					header("Content-type: image/gif");
					$res = imagegif($this->getImg());
					break;

				default:
					$res = NULL;
			}

			imagedestroy($this->getImg());

			return $res;
		}

		public function save($type,$path)
		{
			switch($type)
			{
				case "JPG" :
					$res = imagejpeg($this->getImg(),$path);
					break;

				case "PNG" :
					$res = imagepng($this->getImg(),$path);
					break;

				case "GIF" :
					$res = imagegif($this->getImg(),$path);
					break;

				default:
					$res = NULL;
			}

			return $res;
		}



		// PRIVATE METHODS

		private function flip_h()
		{
			$r = imagecreatetruecolor($this->getWidth(),$this->getHeight());
			$x_i = $this->getWidth();
			$y_i = $this->getHeight();

			for($x=0 ; $x<$x_i ; $x++)
			{
				for($y=0 ; $y<$y_i ; $y++)
				{
					imagecopy($r, $this->getImg(), $x_i-$x-1, $y, $x, $y, 1, 1);
				}
			}

			$this->setImg($r);
		}

		private function flip_v()
		{
			$r = imagecreatetruecolor($this->getWidth(),$this->getHeight());
			$x_i = $this->getWidth();
			$y_i = $this->getHeight();

			for($x=0 ; $x<$x_i ; $x++)
			{
				for($y=0 ; $y<$y_i ; $y++)
				{
					imagecopy($r, $this->getImg(), $x, $y_i-$y-1, $x, $y, 1, 1);
				}
			}

			$this->setImg($r);
		}



		// STATIC METHODS

		public static function create_image_from($file,$type)
		{
			switch($type)
			{
				case 1 : // gif
					$handler = imagecreatefromgif($file);
					break;

				case 2 : // jpeg
					$handler = imagecreatefromjpeg($file);
					break;

				case 3 : // png
					$handler = imagecreatefrompng($file);
					break;
			}

			return $handler;
		}

		public static function hex2rgb($h)
		{
			$int = hexdec($h);
			$r = ($int >> 16) & 255;
			$v = ($int >> 8) & 255;
			$b = ($int) & 255;

			$res = array(
				"r" => $r,
				"v" => $v,
				"b" => $b,
			);

			return $res;
		}
	}
?>