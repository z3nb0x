<?php
	$Z3NB0X_CONFIG = parse_ini_file("Z3NB0X.ini");

	$_Z3N__INSTALL_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"];
	$_Z3N__URL = $Z3NB0X_CONFIG["URL"];
	$_Z3N__THEME = $Z3NB0X_CONFIG["THEME"];
	$_Z3N__LANG_I18N = $Z3NB0X_CONFIG["I18N"];



	/*
	 *************************************************************
	 *
	 *		DO NOT EDIT BEHIND THIS LINE
	 *
	 *	     UNLESS YOU REALLY KNOW WHAT YOU DO
	 *
	 *************************************************************
	 */

	// Path to the cache directory that will be used by z3nb0x :
	$Z3NB0X_CONFIG["CACHE_PATH"] = $Z3NB0X_CONFIG["INSTALL_PATH"]."/cache";
	$_Z3N__CACHE_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"]."/cache";

	// Path to the media directory :
	$Z3NB0X_CONFIG["MEDIA_PATH"] = $Z3NB0X_CONFIG["INSTALL_PATH"]."/media";
	$_Z3N__MEDIA_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"]."/media";

	// Path to the plugins directory :
	$_Z3N__PLUGINS_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"]."/_modules";

	// Path to the themes directory :
	$Z3NB0X_CONFIG["THEMES_PATH"] = $Z3NB0X_CONFIG["INSTALL_PATH"]."/_themes";
	$_Z3N__THEMES_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"]."/_themes";

	// Path to the updates :
	$_Z3N__UPDATES_PATH = $Z3NB0X_CONFIG["INSTALL_PATH"]."/_updates";
?>