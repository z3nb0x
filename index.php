<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/_inc/z3nb0x.class.php");
	require_once(dirname(__FILE__)."/_inc/z3nErrorManager.class.php");





	// SETTING UP A NEW Z3NB0X _____________________________________________

	$z3nb0x = new z3nb0x();






	// CHECKING OUT PHP SETTINGS ___________________________________________

	// Drops an error if magic_quotes_gpc is On :
	if(function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc())
	{
		$msg = dgettext("z3nb0x", "Bad PHP config !");
		$exp = dgettext("z3nb0x", "I've detected that the PHP directive called <i>magic_quotes_gpc</i> is set to <i>On</i>. Please set it to <i>Off</i> in your <i>php.ini</i> file.");

		$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
	}





	// CHECKING OUT USER DEFINED SETTINGS __________________________________

	// Drops an error if z3nb0x path is empty :

	$p = $z3nb0x->getInstallPath();

	if(empty($p))
	{
		$msg = dgettext("z3nb0x", "Bad config !");
		$exp = dgettext("z3nb0x", "Hey, I need to know where z3nb0x is installed. Please open the <i>Z3NB0X.ini</i> file and fill in your preferences.");

		$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
	}
	else
	{
		// Drops an error if z3nb0x path cannot be found :
		if(!is_dir($z3nb0x->getInstallPath()))
		{
			$msg = dgettext("z3nb0x", "Bad config !");
			$exp = sprintf(dgettext("z3nb0x", "I wasnt able to find where z3nb0x is installed. Are you sure it is in %s ?"), "<i>".$z3nb0x->getInstallPath()."</i>");

			$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
		}
	}


	// Drops an error if z3nb0x cache path is empty :

	$c = $z3nb0x->getCachePath();

	if(empty($c))
	{
		$msg = dgettext("z3nb0x", "Bad config !");
		$exp = dgettext("z3nb0x", "Hey, I need to know where you want me to store cached files. Please open the <i>Z3NB0X.ini</i> file and fill in your preferences.");

		$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
	}
	else
	{
		// Drops an error if z3nb0x cache path cannot be found :
		if(!is_dir($z3nb0x->getCachePath()))
		{
			$msg = dgettext("z3nb0x", "Bad config !");
			$exp = sprintf(dgettext("z3nb0x", "I wasn't able to find your cache directory. Are you sure it is in %s ?"), "<i>".$z3nb0x->getCachePath()."</i>");

			$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
		}
		else
		{
			// Drops an error if z3nb0x cache path is not writeable :
			if(!is_writeable($z3nb0x->getCachePath()."/"))
			{
				$msg = dgettext("z3nb0x", "Bad config !");
				$exp = sprintf(dgettext("z3nb0x", "I wasn't able to write in your cache directory (%s). Please chmod it so I can write in it."), "<i>".$z3nb0x->getCachePath()."</i>");

				$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
			}
		}
	}


	// Drops an error if no theme were selected

	$t = $z3nb0x->getTheme();

	if(empty($t))
	{
		$msg = dgettext("z3nb0x", "Bad config !");
		$exp = dgettext("z3nb0x", "Don't you want z3nb0x to look nice ? Please open the <i>Z3NB0X.ini</i> file and fill in your preferences.");

		$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
	}
	else
	{
		// Drops an error if chosen theme doesn't exist :
		if(!is_dir("_themes/".$z3nb0x->getTheme()))
		{
			$msg = dgettext("z3nb0x", "Bad config !");
			$exp = sprintf(dgettext("z3nb0x", "I wasn't able to detect the theme you have chosen (%s). Please verify that the theme really exists in %s."), "<i>".$z3nb0x->getTheme()."</i>", "<i>".$z3nb0x->getInstallPath()."/_themes/</i>");

			$z3nb0x->getErrorManager()->addError(new z3nError($msg, $exp));
		}
	}





	// OKAY, LET'S DISPLAY SOMETHING _______________________________________

	$z3nb0x->display();
?>