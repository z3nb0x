<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $this->getName(); ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $this->getTheme(); ?>/css/general.css" media="screen" />
</head>
<body>
	<h1><?php echo $this->getName(); ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $this->getPicture(); ?>" />

	<div id="main">
		<div class="error">
			<p class="msg">
				<?php echo $err->getMessage(); ?>
			</p>
			<p class="explanation">
				<?php echo $err->getExplanation(); ?>
			</p>
		</div>
	</div>

	<a id="back" href="<?php echo $this->getBacklink(); ?>">&lt;</a>
</body>
</html>