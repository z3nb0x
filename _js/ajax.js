
function getXHR()
{
	var xhr_object = null;

	if(window.XMLHttpRequest)						// Objet XmlHttpRequest pour les moteurs GECKO & IE7
	{
		xhr_object = new XMLHttpRequest();
		//xhr_object.overrideMimeType("text/xml");
	}
	else if(window.ActiveXObject)						// Objet XmlHttpRequest pour Internet Explorer
	{
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else									// Navigateur incompatible
	{
		alert("Votre navigateur ne supporte pas la technologie AJAX...");
	}

	return xhr_object;
}



function ajax_do(url, callback, sync)
{
	var xhr = getXHR();

	xhr.open("GET", url, sync);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(null);

	xhr.onreadystatechange = function()
	{
		try
		{
			if(xhr.readyState==4 && xhr.status==200)
			{
				if(callback != undefined && typeof callback == 'function')
				{
					callback(xhr.responseText);
				}
			}
		}
		catch(error)
		{
			//alert("Une exception s'est produite :\r\n" + error);
		}
	}
}


function ajax_doXML(url, callback, sync)
{
	var xhr = getXHR();

	xhr.open("GET", url, sync);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(null);

	xhr.onreadystatechange = function()
	{
		try
		{
			if(xhr.readyState==4 && xhr.status==200)
			{
				if(callback != undefined && typeof callback == 'function')
				{
					callback(xhr.responseXML);
				}
			}
		}
		catch(error)
		{
			//alert("Une exception s'est produite :\r\n" + error);
		}
	}
}



// -------------------------------------------------------------------------------------------------
// COMMON USED AJAX FUNCTION
// -------------------------------------------------------------------------------------------------

function go_to(msg,p)
{
	showLoading(msg);

	ajax_do(p, afficheHTML, true);
}



function request_and_stay(msg,p,sync)
{
	if(msg!="")
		showLoading(msg);

	ajax_do(p, "", sync);

	if(msg!="")
		closeLoading;
}



// -------------------------------------------------------------------------------------------------
// COMMON CALLBACK FUNCTIONS
// -------------------------------------------------------------------------------------------------

function afficheHTML(p)
{
	document.body.innerHTML = p;
}



// -------------------------------------------------------------------------------------------------
// LOADING FUNCTIONS
// -------------------------------------------------------------------------------------------------

function showOverlay()
{
	var objOverlay = document.createElement("div");
	objOverlay.setAttribute("id","overlay");

	document.body.appendChild(objOverlay);
}



function closeOverlay()
{
	try
	{
		var objOverlay = document.getElementById("overlay");
		document.body.removeChild(objOverlay);
	}
	catch(err)
	{
		//alert("closeOverlay : \r\n"+err);
	}
}



function showLoading(msg)
{
	showOverlay();

	var objLoad = document.createElement("div");
	var objMsg = document.createElement("p");
	var objText = document.createTextNode(msg);

	objLoad.setAttribute("class","loading");
	objLoad.setAttribute("id","loading");

	objMsg.appendChild(objText);
	objLoad.appendChild(objMsg);

	try
	{
		var objOverlay = document.getElementById("overlay");
		objOverlay.appendChild(objLoad);
	}
	catch(err)
	{
		//alert("showLoading : \r\n"+err);
	}
}



function closeLoading()
{
	closeOverlay();

	try
	{
		var objLoad = document.getElementById("loading");
		document.body.removeChild(objLoad);
	}
	catch(err)
	{
		//alert("closeLoading : \r\n"+err);
	}
}
