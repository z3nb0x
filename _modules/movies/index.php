<?php
	// Module import _______________________________________________________

	require_once(dirname(__FILE__)."/MoviesModule.class.php");

	// Let's create a new instance of module
	$module = new MoviesModule(dirname(__FILE__)."/index.tpl.php");



	// i18n ________________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/i18n2.inc.php");

	bindtextdomain("movies",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("movies","UTF-8");



	// Module creation _____________________________________________________

	// Then, build the data ________________________________________________
	$module->buildModule();

	// And finally, display it _____________________________________________
	$module->display();
?>