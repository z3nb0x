<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $this->getName(); ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $this->getTheme(); ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
	<script language="javascript" type="text/javascript" src="../../_js/ajax.js"></script>
	<script language="javascript" type="text/javascript" src="_js/imageflow.js"></script>
</head>
<body>
	<h1><?php echo $this->getName(); ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $this->getPicture(); ?>" />

	<div id="overlay">
		<div id="loading" class="loading">
			<p><?php echo $_DATA["LOADING"]; ?></p>
		</div>
	</div>

	<div id="imageflow">
		<div id="images">
			<?php foreach($this->getMovies() as $m): ?>
				<img src="create_cover.php?cover=<?php echo $m["COVER"]; ?>" alt="<?php echo $m["NAME"]; ?>" rel="<?php echo $m["FILE"]; ?>" />
			<?php endforeach; ?>
		</div>
		<div id="captions"></div>
		<div id="scrollbar">
			<div id="slider"></div>
		</div>
	</div>

	<a id="back" href="<?php echo $this->getBacklink(); ?>">&lt;</a>
</body>
</html>