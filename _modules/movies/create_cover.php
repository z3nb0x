<?php
	require_once(dirname(__FILE__)."/MoviesModule.class.php");
	require_once(dirname(__FILE__)."/../../_inc/Image.class.php");

	// We need to specify a template file, even if it isn't necessary for this file.
	$module = new MoviesModule(__FILE__);


	if(isset($_GET["cover"]) && !empty($_GET["cover"]))
	{
		$img = urldecode($_GET["cover"]);
		$img_sum = "movies_".md5($img).".png";
		$img_path = $module->getCachePath()."/".$img_sum;

		if(file_exists($img_path) && filemtime($img_path) >= filemtime($img))
		{
			// We use the cached image

			header("Content-type: image/png");
			readfile($img_path);
			exit();
		}
		else
		{
			// We build a new image

			$pic = new Image($img);
			$pic->resize($module->getCoverWidth(), $module->getCoverHeight());
			$pic->create_reflection($module->getCoverReflectionOpacity(),$module->getCoverReflectionPercentage());

			$pic->save("PNG",$img_path);
			//chown($cover_path,"z3nb0x");
			//chgrp($img_path,"z3nb0x");
			chmod($img_path,0755);
			$pic->draw("PNG");
		}
	}
	else
	{
		exit();
	}
?>