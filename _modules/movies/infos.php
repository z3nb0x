<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");
	require_once(dirname(__FILE__)."/../moviecovers/MovieCoversFilm.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");
	$exts = explode("|", $MODULE_CONFIG["EXT"]);





	// i18n ________________________________________________________________

	bindtextdomain("movies",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("movies","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("movies", "Movies"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "index.php";


	// OK, now, the main part

	if(isset($_GET["movie"]) && !empty($_GET["movie"]))
	{
		$movie = urldecode($_GET["movie"]);

		$infos_p = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["INFOS_PATH"]."/";
		$cover_p = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["COVERS_PATH"]."/";

		$pathinf = $infos_p.basename($movie).".txt";
		$pathimg = $cover_p.basename($movie).".jpg";

		$_DATA["FILE"] = urlencode($movie);


		// Cover

		$cover = $Z3NB0X_CONFIG["URL"]."/".$MODULE_CONFIG["DEFAULT_COVER"];

		if(file_exists($pathimg))
		{
			$cover = $Z3NB0X_CONFIG["URL"]."/".basename($Z3NB0X_CONFIG["MEDIA_PATH"])."/".$MODULE_CONFIG["COVERS_PATH"]."/".basename($movie).".jpg";
		}


		// Infos

		if(file_exists($pathinf))
		{
			$inf = file_get_contents($pathinf);
			$f = new MovieCoversFilm("","",$inf);

			$_DATA["TXT"] = array(
				"REAL" => dgettext("movies","Director(s)"),
				"ACT" => dgettext("movies","Actors"),
				"SYNOPSIS" => dgettext("movies","Synopsis"),
			);

			$_DATA["MOVIE"] = array(
				"TITLE" => $f->_get__titre(),
				"LENGTH" => $f->_get__duree(),
				"FROM" => $f->get_nationalites_as_string(),
				"GENRE" => $f->get_genres_as_string(),
				"YEAR" => $f->_get__annee(),
				"REAL" => $f->get_realisateurs_as_string(),
				"ACT" => $f->get_acteurs_as_string(),
				"SYNOPSIS" => $f->_get__synopsis(),
			);

			$_DATA["MOVIE"] = array_map("htmlentities", $_DATA["MOVIE"]);
			$_DATA["MOVIE"]["COVER"] = htmlspecialchars($cover);
		}
		else
		{
			$_DATA["ERR2"] = array(
				"ERROR" => htmlentities(basename(utf8_decode($movie), ".".Browser::get_extension($movie))),
				"EXPLANATION" => dgettext("movies", "Sorry, I wasn't able to find any information for this movie !"),
				"GET_INFOS" => dgettext("movies", "Try to get info from MovieCovers.com"),
				"PLAY_MOVIE" => dgettext("movies", "Play this movie"),
			);
		}
	}
	else
	{
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("video", "No file specified in request !"),
			"EXPLANATION" => dgettext("video", "You are trying to display information about...nothing..."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]) && !isset($_DATA["ERR2"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/infos.tpl.php");
	}
	else if(isset($_DATA["ERR2"]))
	{
	// An error occured, but we can go on :)
		include(dirname(__FILE__)."/infos_no.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.tpl.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>