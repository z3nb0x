<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style_infos.css" media="screen" />
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<div class="error">
			<p class="msg">
				<?php echo $_DATA["ERR2"]["ERROR"]; ?>
			</p>
			<p class="explanation">
				<?php echo $_DATA["ERR2"]["EXPLANATION"]; ?>
			</p>
			<hr />
			<ul>
				<li><a href="../moviecovers/index.php?movie=<?php echo $_DATA["FILE"]; ?>"><?php echo $_DATA["ERR2"]["GET_INFOS"]; ?></a></li>
				<li><a href="play.php?movie=<?php echo $_DATA["FILE"]; ?>"><?php echo $_DATA["ERR2"]["PLAY_MOVIE"]; ?></a></li>
			</ul>
		</div>
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>