/*
 *
 */

//var _icones_path = "../../_themes/z3nGrass/images/player/";
var _icones_path;
var vlc;

function play()
{
	vlc.playlist.play();
	buttons();

	return false;
}

function pause()
{
	vlc.playlist.togglePause();
	buttons();

	return false;
}

function stop()
{
	vlc.playlist.stop();
	buttons();

	return false;
}

function fullscreen()
{
	vlc.video.toggleFullscreen();

	return false;
}

function buttons()
{
	if(vlc.playlist.isPlaying)
	{
		document.getElementById("img_play").src = _icones_path+"pause.png";
	}
	else
	{
		document.getElementById("img_play").src = _icones_path+"play.png";
	}
}

function controler()
{
	// We get the VLC object :
	vlc = document.getElementById("vlc");

	// We get the theme (for the buttons) :
	_icones_path = "../../_themes/"+document.getElementById("theme").value+"/images/player/";

	// What the buttons do :
	document.getElementById("btn_play").onclick = function()
	{
		if(vlc.playlist.isPlaying)
		{
			pause();
		}
		else
		{
			play();
		}

		return false;
	}

	document.getElementById("btn_stop").onclick = function()
	{
		stop();
		return false;
	}

	document.getElementById("fullscreen").onclick = function()
	{
		fullscreen();
		return false;
	}

	play();
	setTimeout("fullscreen()",3000);
}

window.onload = function()
{
	controler();
}
