<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style_infos.css" media="screen" />
</head>
<body class="opaque">
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<div id="movie_cover">
			<a href="play.php?movie=<?php echo $_DATA["FILE"]; ?>" class="link_cover">
				<img src="<?php echo $_DATA["MOVIE"]["COVER"]; ?>" alt="<?php echo $_DATA["MOVIE"]["TITLE"]; ?>" class="cover" />
			</a>
		</div>

		<dl id="movie_info">
			<dt><?php echo $_DATA["MOVIE"]["TITLE"]; ?></dt>
			<dd><?php echo $_DATA["MOVIE"]["GENRE"]; ?> - Film <?php echo $_DATA["MOVIE"]["FROM"]; ?> (<?php echo $_DATA["MOVIE"]["YEAR"]; ?>)</dd>
			<dd><?php echo $_DATA["MOVIE"]["LENGTH"]; ?></dd>
			<dd><span><?php echo $_DATA["TXT"]["REAL"]; ?>&nbsp;:&nbsp;</span><?php echo $_DATA["MOVIE"]["REAL"]; ?></dd>
			<dd><span><?php echo $_DATA["TXT"]["ACT"]; ?>&nbsp;:&nbsp;</span><?php echo $_DATA["MOVIE"]["ACT"]; ?></dd>
			<dd class="resume"><span><?php echo $_DATA["TXT"]["SYNOPSIS"]; ?>&nbsp;:&nbsp;</span><?php echo $_DATA["MOVIE"]["SYNOPSIS"]; ?></dd>
		</dl>
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>