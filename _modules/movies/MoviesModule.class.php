<?php
	require_once(dirname(__FILE__)."/../../_inc/Module.class.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");
	require_once(dirname(__FILE__)."/../../_inc/z3nErrorManager.class.php");



	class MoviesModule extends Module
	{
		// ATTRIBUTES

		const CLASS_VERSION = "20080430";		// Version of this file (must be a yyyymmdd formated date).

		protected $movies;				// Array	: Movies.

		protected $rootdir;				// String	: Path to the movies.
		protected $extensions;				// Array	: Supported file extensions.

		protected $infosPath;				// String	: Path to the directory where info files are stored.
		protected $coversPath;				// String	: Path to the directory where cover pictures are stored.
		protected $defaultCover;			// String	: Path to the default cover picture.

		protected $coverWidth;				// int		: Max width of cover picture, in pixels.
		protected $coverHeight;				// int		: Max height of cover picture, in pixels.
		protected $coverReflectionOpacity;		// int		: Opacity of the reflection effect.
		protected $coverReflectionPercentage;		// int		: Height of picture that will be reflected, expressed as a percentage of the original picture height.



		// CONSTRUCTOR

		public function __construct($tpl_file)
		{
			parent::__construct($tpl_file);

			// Data to display will be stored here :
			$movies = array();

			// Sets the name of plugin :
			$this->setName(dgettext("movies", "Movies"));

			// Read the configuration from MODULE.ini file :
			$ini = $this->getModuleConfig();

			$this->setRootdir($this->getMediaPath()."/".$ini["ROOTDIR"]);

			$this->setInfosPath($ini["INFOS_PATH"]);
			$this->setCoversPath($ini["COVERS_PATH"]);
			$this->setDefaultCover($ini["DEFAULT_COVER"]);

			$this->setCoverWidth($ini["WIDTH"]);
			$this->setCoverHeight($ini["HEIGHT"]);
			$this->setCoverReflectionOpacity($ini["REFL_OPACITY"]);
			$this->setCoverReflectionPercentage($ini["REFL_PERC"]);

			$ext = explode("|", $ini["EXT"]);
			$this->setExtensions($ext);
		}


		// PUBLIC METHODS

		public function getClassVersion()
		{
			return self::CLASS_VERSION;
		}



		/*
		 * buildModule :
		 *
		 * 	This method is used to set the $backlink and the $movies attributes.
		 *
		 */
		public function buildModule()
		{
			// $backlink :
			$this->buildBacklink();

			// $movies :
			$this->buildMovies();

		}





		// PRIVATE METHODS

		private function buildBacklink()
		{
			$this->setBacklink("../../_menus/menu.php?menu=video");
		}


		private function buildMovies()
		{
			try
			{
				$browser = new Browser($this->getRootdir());

				$browser->delete_dir(".");
				$browser->delete_dir("..");
				$browser->hide_hidden_files();
				$browser->order_files(Browser::SORT_ASC);
				$browser->filter($this->getExtensions());

				$files = $browser->getFiles();

				if(count($files) > 0)
				{
					foreach($files as $f)
					{
						$cover_path = $this->getMediaPath()."/".$this->getCoversPath()."/".$f.".jpg";

						if(file_exists($cover_path))
							$cover_url = $cover_path;
						else
							$cover_url = $this->getDefaultCover();

						$m = array(
							"FILE" => urlencode($this->getRootdir()."/".$f),
							"NAME" => htmlentities(utf8_decode(basename($f,".".Browser::get_extension($f)))),
							"COVER" => urlencode($cover_url),
						);

						$this->addMovie($m);
					}
				}
				else
				{
					$msg = dgettext("movies", "This directory is empty !");
					$exp = dgettext("movies", "Please ensure that your files extensions are not too restrictive in your <i>MODULE.ini</i> file.");

					$this->getErrorManager()->addError(new z3nError($msg, $exp));
				}
			}
			catch(Exception $e)
			{
				$msg = dgettext("movies", "This directory doesn't exist !");
				$exp = sprintf(dgettext("movies", "I'm sorry, I wasn't able to open <i>%s</i> directory. Please ensure that it exists and that I'm allowed to browse it :)"), htmlentities($this->getRootdir()));

				$this->getErrorManager()->addError(new z3nError($msg, $exp));
			}
		}


		private function addMovie($m)
		{
			$movies = $this->getMovies();
			$movies[] = $m;

			$this->setMovies($movies);
		}
	}
?>