<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");
	$exts = explode("|", $MODULE_CONFIG["EXT"]);





	// i18n ________________________________________________________________

	bindtextdomain("movies",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("movies","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("movies", "Movies"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the (default) BACK_LINK

	$_DATA["BACK_LINK"] = $_SERVER["HTTP_REFERER"];


	// OK, now, the main part

	if(isset($_GET["movie"]) && !empty($_GET["movie"]))
	{
		// Something has been asked :

		$open = "file://".urldecode($_GET["movie"]);

		if(file_exists($open))
		{
			// Allright, the file exists :
			$_DATA["SRC"] = htmlspecialchars($open);
		}
		else
		{
			// The file doesn't exist :
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("movies", "File not found !"),
				"EXPLANATION" => dgettext("movies", "It seems you are trying to play a file that doesn't exist..."),
			);
		}
	}
	else
	{
		// Nothing was asked for, you should not be here !
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("movies", "No file specified in request !"),
			"EXPLANATION" => dgettext("movies", "You are trying to play...nothing..."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/play.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>