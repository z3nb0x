<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<div id="dir">
		<?php foreach($_DATA["FOLDERS"] as $folder) :?>
			<dl class="seasons">
				<dt>	<a class="<?php echo $_DATA["CLASS"]; ?>" href="episodes.php?serie=<?php echo $_DATA["FOLDERS"]["SERIE"]; ?>&saison=<?php echo $_DATA["FOLDERS"]["SEASON"]; ?>" title="<?php echo $_DATA["FOLDERS"]["NAME"]; ?>">
						<img src="<?php echo $_DATA["FOLDERS"]["IMG"]" alt="<?php echo $_DATA["FOLDERS"]["NAME"]; ?>" />
					</a>
				</dt>
				<dd><?php echo $_DATA["FOLDERS"]["NAME"]; ?></dd>
			</dl>
		<?php endforeach;?>
		</ul>
		</div>
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>