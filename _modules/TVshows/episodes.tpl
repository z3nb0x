<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?> <span><?php echo $_DATA[""]; ?></span></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<dl class="seasons">
			<dt><img src="<?php echo $_DATA["COVER"]; ?>" alt="<?php echo $_DATA[""]; ?>" /></dt>
		</dl>

		<ul class="dir_episodes">
			<?php foreach($_DATA["FILES"] as $f): ?>
			<li>
				<a href="../movies/play.php?movie=<?php echo $f["PATH"]; ?>/<?php echo $f["FILE"]; ?>" title="<?php echo $f["NAME"]; ?>">
					<?php echo $f["NAME"]; ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>