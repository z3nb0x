<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");
	$MOVIES_MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/../movies/MODULE.ini");
	$extensions = explode("|", $MOVIES_MODULE_CONFIG["EXT"]);





	// i18n ________________________________________________________________

	bindtextdomain("TVshows",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("TVshows","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("TVshows", "TV Shows"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// Let's see build the defult back link :

	$_DATA["BACK_LINK"] = "seasons.php?serie=".urlencode($_GET["serie"]);


	// Then, we have to build the path to the given folder

	$serie = urldecode($_GET["serie"]);
	$saison = urldecode($_GET["saison"]);

	$open = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"]."/".$serie."/".$saison;


	// Ok, let's try to get the content of the folder, and see if you have pictures in it

	try
	{
		$browser = new Browser($open);

		$browser->hide_hidden_files();
		$browser->order_files(Browser::SORT_ASC);
		$browser->filter($extensions);

		$files = $browser->getFiles();

		if(count($files)>0)
		{
			$_DATA["COVER"] = htmlspecialchars($Z3NB0X_CONFIG["URL"]."/".basename($Z3NB0X_CONFIG["MEDIA_PATH"])."/".$MODULE_CONFIG["ROOTDIR"]."/".$serie."/".$saison."/folder.jpg");
			$_DATA["FILES"] = array();

			foreach($files as $f)
			{
				$_DATA["FILES"][] = array(
					"PATH" => urlencode($open),
					"NAME" => htmlentities(utf8_decode(basename($f,".".Browser::get_extension($f)))),
					"FILE" => urlencode($f),
				);
			}
		}
		else
		{
		// The folder seems to be empty
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("TVshows", "This directory is empty !"),
				"EXPLANATION" => dgettext("TVshows", "It seems like there is nothing in this directory..."),
			);
		}
	}
	catch(Exception $e)
	{
	// Seems like the folder doesn't exist
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("TVshows","Unable to open directory !"),
			"EXPLANATION" => sprintf(dgettext("TVshows","Please ensure that I'm allowed to browse this directory : <i>%s</i> !"), $open),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/episodes.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>