﻿<?php
	$_SERIES__PLUGIN_IMG = "plugin.png";			// Path de l'image du plugin
	$_SERIES__ROOT = "video/series";			// Path par défaut (sans "/" de fin)




	$_SERIES__ERR_NO_FILE = "Ce dossier est vide !";		// Message displayed when the browsed dir is empty
	$_SERIES__ERR_NO_FILE_EXPLANATION = "Assurez-vous d'avoir bien configuré les filtres dans le fichier <i>config.inc.php</i> du plugin Series :)";

	$_SERIES__ERR_WRONG_PATH = "Ce dossier n'existe pas !";	// Message displayed when the directory does not exist
	$_SERIES__ERR_WRONG_PATH_EXPLANATION = "Assurez-vous d'avoir renseigné correctement le fichier <i>config.inc.php</i> du plugin Series :)";

	$_SERIES__EXTENSIONS = array(
		"avi",
		"mpe",
		"mpg",
		"mpeg",
		"mkv",
	);
?>
