<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<ul id="dir">
		<?php foreach($_DATA["FOLDERS"] as $folder) :?>
		<li class="<?php $_DATA["CLASS"]; ?>">
			<a href="seasons.php?serie=<?php echo $folder["PATH"]; ?>" title="<?php echo $folder["NAME"]; ?>">
				<img src="<?php echo $folder["IMG"]; ?>" alt="<?php $_DATA["NAME"]; ?>" />
			</a>
		</li>
		<?php endforeach;?>
		</ul>
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>








<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:series]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="shortcut icon" href="../../favicon.ico" />
		<link rel="stylesheet" type="text/css" href="../../_themes/{THEME}/css/general.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1>{PLUGIN_TITLE}</h1>
	<img class="plugin_img" alt="" src="{PLUGIN_IMG}" />

	<div id="main">
		<!-- BEGIN ok -->
		<div id="dir">
			<ul class="dir">
			<!-- BEGIN serie -->
				<li><a class="{CLASS}" href="seasons.php?serie={PATH}" title="{NAME}"><img src="{IMG}" alt="{NAME}" /></a></li>
			<!-- END serie -->
			</ul>
		</div>
		<!-- END ok -->

		<!-- BEGIN err -->
		<div class="error">
			<p class="msg">
				{ERROR}
			</p>
			<p class="explanation">
				{EXPLANATION}
			</p>
		</div>
		<!-- END err -->
	</div>

	<a id="back" href="../../_menus/menu.php?menu=video">&lt;</a>
</body>
</html>
