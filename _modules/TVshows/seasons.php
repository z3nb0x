<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("TVshows",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("TVshows","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("TVshows", "TV Shows"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// Let's see build the defult back link :

	$_DATA["BACK_LINK"] = "index.php";


	// Then, we have to build the path to the given folder

	$serie = urldecode($_GET["serie"]);

	$open = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"]."/".$serie;


	// Ok, let's try to get the content of the folder, and see if you have pictures in it

	try
	{
		$browser = new Browser($open);

		$browser->delete_dir(".");
		$browser->delete_dir("..");
		$browser->hide_hidden_dirs();
		$browser->order_dirs("ASC");

		$dirs = $browser->getDirs();

		if(count($dirs)>0)
		{
			$_DATA["FOLDERS"] = array();

			foreach($dirs as $d)
			{
				$class="normal";

				$img = $Z3NB0X_CONFIG["URL"]."/".basename($Z3NB0X_CONFIG["MEDIA_PATH"])."/".$MODULE_CONFIG["ROOTDIR"]."/".$serie."/".$d."/folder.jpg";

				if(!file_exists($img))
				{
					$class = "na";
				}

				$_DATA["FOLDERS"][] = array(
					"SERIE" => urlencode($serie),
					"SEASON" => urlencode($d),
					"NAME" => htmlentities(utf8_decode($d)),
					"CLASS" => $class,
					"IMG" => htmlspecialchars($img),
				);
			}
		}
		else
		{
		// The folder seems to be empty
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("TVshows", "This directory is empty !"),
				"EXPLANATION" => dgettext("TVshows", "It seems like there is nothing in this directory..."),
			);
		}
	}
	catch(Exception $e)
	{
	// Seems like the folder doesn't exist
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("TVshows","Unable to open directory !"),
			"EXPLANATION" => sprintf(dgettext("TVshows","Please ensure that I'm allowed to browse this directory : <i>%s</i> !"), $open),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/seasons.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>