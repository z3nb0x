<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
	<script type="text/javascript" src="../../_js/ajax.js"></script>
	<script type="text/javascript" src="_js/z3nGallery.js"></script>
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="main">
		<ul id="dir">
		<?php foreach($_DATA["FOLDERS"] as $folder): ?>
		<li class="folder"><a href="?path=<?php echo $folder["PATH"]; ?>" title="<?php echo $folder["NAME"]; ?>"><?php echo $folder["NAME"]; ?></a></li>
		<?php endforeach;?>

		<?php foreach($_DATA["FILES"] as $file) :?>
		<li class="photo"><a href="<?php echo $file["IMG"]; ?>" title="<?php echo $file["NAME"]; ?>" rel="zss"><img src="create_mini.php?img=<?php echo $file["PATH"]; ?>" alt="<?php echo $file["NAME"]; ?>" /></a></li>
		<?php endforeach;?>
		</ul>

		<input type="hidden" id="theme" name="theme" value="<?php echo $_DATA["THEME"]; ?>" />
	</div>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>