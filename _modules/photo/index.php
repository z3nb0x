<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");
	$exts = explode("|", $MODULE_CONFIG["EXT"]);





	// i18n ________________________________________________________________

	bindtextdomain("photo",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("photo","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("photo", "Photos"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// Then, we have to build the path to the given folder
	// Notice that we build the BACK_LINK at the same time

	$open = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"];

	if(isset($_GET["path"]))
	{
		if($_GET["path"]=="/" || empty($_GET["path"]))
		{
			$_DATA["BACK_LINK"] = "../../_menus/menu.php?menu=medias";
			$_GET["path"] = "";
		}
		else
		{
			$open .= urldecode($_GET["path"]);
			$_DATA["BACK_LINK"] = htmlentities("index.php?path=".dirname(urldecode($_GET["path"])));
		}
	}
	else
	{
		$_DATA["BACK_LINK"] = "../../_menus/menu.php?menu=medias";
	}


	// Ok, let's try to get the content of the folder, and see if you have pictures in it

	try
	{
		$browser = new Browser($open);

		$browser->delete_dir(".");
		$browser->delete_dir("..");
		$browser->hide_all();
		$browser->filter($exts);

		$dirs = $browser->getDirs();
		$files = $browser->getFiles();

		$_DATA["FOLDERS"] = array();
		$_DATA["FILES"] = array();

		if(count($dirs) > 0)
		{
		// We have some folders
			$browser->order_dirs(Browser::SORT_ASC);
			$dirs = $browser->getDirs();

			foreach($dirs as $d)
			{
				$p = urldecode($_GET["path"])."/".$d;

				$_DATA["FOLDERS"][] = array(
					"PATH" => urlencode($p),
					"NAME" => htmlentities(utf8_decode($d)),
				);
			}
		}

		if(count($files) > 0)
		{
		// We have some files
			$browser->order_files(Browser::SORT_ASC);
			$files = $browser->getFiles();

			foreach($files as $f)
			{
				$p = $open."/".$f;
				$n = utf8_encode($f);

				$img = $Z3NB0X_CONFIG["URL"]."/".basename($Z3NB0X_CONFIG["MEDIA_PATH"])."/".$MODULE_CONFIG["ROOTDIR"].urldecode($_GET["path"])."/".$f;

				$_DATA["FILES"][] = array(
					"IMG" => htmlentities(utf8_decode($img)),
					"PATH" => urlencode($p),
					"NAME" => htmlentities(utf8_decode($n)),
				);
			}
		}

		if(!(count($files)>0) && !(count($dirs)>0))
		{
		// There is nothing interesting in this folder (it could be either empty or there is no picture in it)
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("photo","This directory is empty !"),
				"EXPLANATION" => dgettext("photo", "Please ensure that your filters are not to restrictive in you <i>MODULE.ini</i> file."),
			);
		}
	}
	catch(Exception $e)
	{
	// Seems like the folder doesn't exist
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("photo","That directory doesn't exist !"),
			"EXPLANATION" => dgettext("photo", "Please ensure that your <i>MODULE.ini</i> file points to a directory that really exist :)"),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/index.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>