/*
 *** --------------------------------
 *     L I C E N C E
 *** -------------------------------
 *
 *	Creative-Commons Attribution-ShareAlike 2.0 France
 *	Copyright 2006-2007 - Fran�ois Kubler
 *
 *	This file is published under the Attribution-ShareAlike 2.0 France Creative Commons licence.
 *	More details can be found here : http://creativecommons.org/licenses/by-sa/2.0/fr/
 *
 *
 *** --------------------------------
 *     I N F O R M A T I O N
 *** --------------------------------
 *
 *	z3nSlideShow is a small javascript that displays picture in full screen with navigation and diaporama capabilities.
 *
 *	Version :	2007-0420
 *	Author :	Fran�ois Kubler
 *	Contact :	http://ih8tehuman.free.fr/
 *
 *** --------------------------------
 */


/*
 ****************************************************************************
 *	VARIABLES GLOBALES
 ****************************************************************************
 */


 // EDITABLE :

var _delay = 3500;			// Interval of time between 2 pictures in diaporama mode, expressed in ms.


 // DO NOT EDIT :

var _imageArray = new Array();
var _imageActive = 0;
var _overlayed = false;
var _naved = false;
var _diaporama;



/*
 ****************************************************************************
 *	FONCTIONS
 ****************************************************************************
 */



/*
 ----------------------------------------------------------------------------
	zss_initialize() :

		Initializes the array of pictures
 ----------------------------------------------------------------------------
 */

function zss_initialize()
{
	if(document.getElementsByTagName)
	{
		var anchors = document.getElementsByTagName("a");
		var anchors_size = anchors.length;

		for(i=0,j=0 ; i<anchors_size ; i++)
		{
			var anchor = anchors[i];
			var relAttribute = String(anchor.getAttribute("rel"));

			if(anchor.getAttribute("href") && relAttribute.toLowerCase().match("zss"))
			{
				var img = new Image();
				img.src = String(anchor.getAttribute("href"));
				img.name = String(anchor.getAttribute("title"));
				
				_imageArray.push(img);
				
				anchor._index = j;
				anchor.onclick=function()
				{
					showPhoto(this._index);
					showNavigationBar();
					return false;
				}

				j++;
			}
		}
	}
}




/*
 ----------------------------------------------------------------------------
	showNavigationBar() :

		Shows the navigation toolbar
 ----------------------------------------------------------------------------
 */

function showNavigationBar()
{
	if(!_naved)
	{
		var objBody = document.getElementsByTagName("body").item(0);
	
		var nav = document.createElement("ul");
		nav.setAttribute("id","nav");

		var lnav = document.createElement("ul");
		lnav.setAttribute("id","lnav");
		lnav.className = "dontshow";


		var img_first = document.createElement("img");
		img_first.src = "_img/first.png";
		img_first.setAttribute("alt","First photo");
		img_first.setAttribute("id","img_first");

		var img_prev = document.createElement("img");
		img_prev.src = "_img/prev.png";
		img_prev.setAttribute("alt","Previous photo");
		img_prev.setAttribute("id","img_prev");

		var img_play = document.createElement("img");
		img_play.src = "_img/play.png";
		img_play.setAttribute("alt","Start diaporama");
		img_play.setAttribute("id","img_play");

		var img_next = document.createElement("img");
		img_next.src = "_img/next.png";
		img_next.setAttribute("alt","Next photo");
		img_next.setAttribute("id","img_next");

		var img_last = document.createElement("img");
		img_last.src = "_img/last.png";
		img_last.setAttribute("alt","Last photo");
		img_last.setAttribute("id","img_last");

		var img_hide = document.createElement("img");
		img_hide.src = "_img/hide.png";
		img_hide.setAttribute("alt","Hide navigation bar");
		img_hide.setAttribute("id","img_hide");

		var img_show = document.createElement("img");
		img_show.src = "_img/show.png";
		img_show.setAttribute("alt","Show navigation bar");
		img_show.setAttribute("id","img_show");


		var btn_first = document.createElement("a");
		btn_first.setAttribute("id","btn_first");
		btn_first.setAttribute("href","#");
		btn_first.setAttribute("title","Go to first picture of this set");
		btn_first.appendChild(img_first);
		btn_first.onclick = function()
		{
			stop();
			showPhoto(0);
		}
		btn_first.onmouseover = function()
		{
			document.getElementById("img_first").src = "_img/first_hover.png";
		}
		btn_first.onmouseout = function()
		{
			document.getElementById("img_first").src = "_img/first.png";
		}
	
		var btn_prev = document.createElement("a");
		btn_prev.setAttribute("id","btn_prev");
		btn_prev.setAttribute("href","#");
		btn_prev.setAttribute("title","Previous picture");
		btn_prev.appendChild(img_prev);
		btn_prev.onclick = function()
		{
			stop();
			showPhoto(_imageActive-1);
		}
		btn_prev.onmouseover = function()
		{
			document.getElementById("img_prev").src = "_img/prev_hover.png";
		}
		btn_prev.onmouseout = function()
		{
			document.getElementById("img_prev").src = "_img/prev.png";
		}

		var btn_play = document.createElement("a");
		btn_play.setAttribute("id","btn_play");
		btn_play.setAttribute("href","#");
		btn_play.setAttribute("title","Diaporama");
		btn_play.appendChild(img_play);
		btn_play.onclick = function()
		{
			if(_diaporama != undefined)
			{
				stop();
				this.setAttribute("title","Start diaporama");
				document.getElementById("img_play").src = "_img/play.png";
			}
			else
			{
				start()
				this.setAttribute("title","Stop diaporama");
				document.getElementById("img_play").src = "_img/stop.png";
			}
		}
		btn_play.onmouseover = function()
		{
			if(_diaporama != undefined)
				document.getElementById("img_play").src = "_img/stop_hover.png";
			else
				document.getElementById("img_play").src = "_img/play_hover.png";
		}
		btn_play.onmouseout = function()
		{
			if(_diaporama != undefined)
				document.getElementById("img_play").src = "_img/stop.png";
			else
				document.getElementById("img_play").src = "_img/play.png";
		}


		var btn_next = document.createElement("a");
		btn_next.setAttribute("id","btn_next");
		btn_next.setAttribute("href","#");
		btn_next.setAttribute("title","Next picture");
		btn_next.appendChild(img_next);
		btn_next.onclick = function()
		{
			stop();
			showPhoto(_imageActive+1);
		}
		btn_next.onmouseover = function()
		{
			document.getElementById("img_next").src = "_img/next_hover.png";
		}
		btn_next.onmouseout = function()
		{
			document.getElementById("img_next").src = "_img/next.png";
		}


		var btn_last = document.createElement("a");
		btn_last.setAttribute("id","btn_last");
		btn_last.setAttribute("href","#");
		btn_last.setAttribute("title","Go to last picture of this set");
		btn_last.appendChild(img_last);
		btn_last.onclick = function()
		{
			stop();
			showPhoto(_imageArray.length-1);
		}
		btn_last.onmouseover = function()
		{
			document.getElementById("img_last").src = "_img/last_hover.png";
		}
		btn_last.onmouseout = function()
		{
			document.getElementById("img_last").src = "_img/last.png";
		}


		var btn_hide = document.createElement("a");
		btn_hide.setAttribute("id","btn_hide");
		btn_hide.setAttribute("href","#");
		btn_hide.setAttribute("title","Minimize navigation bar");
		btn_hide.appendChild(img_hide);
		btn_hide.onclick = function()
		{
			hideNavigationBar();
		}
		btn_hide.onmouseover = function()
		{
			document.getElementById("img_hide").src = "_img/hide_hover.png";
		}
		btn_hide.onmouseout = function()
		{
			document.getElementById("img_hide").src = "_img/hide.png";
		}


		var btn_show = document.createElement("a");
		btn_show.setAttribute("id","btn_show");
		btn_show.setAttribute("href","#");
		btn_show.setAttribute("title","Show navigation bar");
		btn_show.appendChild(img_show);
		btn_show.onclick = function()
		{
			unHideNavigationBar();
		}
		btn_show.onmouseover = function()
		{
			document.getElementById("img_show").src = "_img/show_hover.png";
		}
		btn_show.onmouseout = function()
		{
			document.getElementById("img_show").src = "_img/show.png";
		}



		var li_first = document.createElement("li");
		li_first.appendChild(btn_first);

		var li_prev = document.createElement("li");
		li_prev.appendChild(btn_prev);

		var li_play = document.createElement("li");
		li_play.appendChild(btn_play);

		var li_next = document.createElement("li");
		li_next.appendChild(btn_next);

		var li_last = document.createElement("li");
		li_last.appendChild(btn_last);

		var li_hide = document.createElement("li");
		li_hide.appendChild(btn_hide);

		var li_show = document.createElement("li");
		li_show.appendChild(btn_show);

		nav.appendChild(li_first);
		nav.appendChild(li_prev);
		nav.appendChild(li_play);
		nav.appendChild(li_next);
		nav.appendChild(li_last);
		nav.appendChild(li_hide);

		lnav.appendChild(li_show);

		objBody.appendChild(nav);
		objBody.appendChild(lnav);

		_naved = true;
	}
}



/*
 ----------------------------------------------------------------------------
	closeNavigationBar() :

		Closes the navigation toolbar
 ----------------------------------------------------------------------------
 */

function closeNavigationBar()
{
	if(_naved)
	{
		try
		{
			var n = document.getElementById("nav");
			document.body.removeChild(n);
			
			var l = document.getElementById("lnav");
			document.body.removeChild(l);
		}
		catch(e)
		{
		}
	
		_naved = false;
	}
}



/*
 ----------------------------------------------------------------------------
	hideNavigationBar() :

		Minimizes the navigation toolbar
 ----------------------------------------------------------------------------
 */

function hideNavigationBar()
{
	document.getElementById("nav").className = "dontshow";
	document.getElementById("lnav").className = "show";
}



/*
 ----------------------------------------------------------------------------
	unHideNavigationBar() :

		Maximizes the navigation toolbar
 ----------------------------------------------------------------------------
 */

function unHideNavigationBar()
{
	document.getElementById("lnav").className = "dontshow";
	document.getElementById("nav").className = "show";
}



/*
 ----------------------------------------------------------------------------
	updateVar(index) :

		Updates some of the global variables
 ----------------------------------------------------------------------------
 */

function updateVar(index)
{
	if(index<0)
		index = _imageArray.length-1;
	
	if(index>_imageArray.length-1)
		index = 0;

	_imageActive = index;
}



/*
 ----------------------------------------------------------------------------
	showPhoto(index) :

		Prepares the picture before displaying it
 ----------------------------------------------------------------------------
 */

function showPhoto(index)
{
	// --- On met � jour les variables :

	updateVar(index);


	// --- On pr�pare l'image :

	var pic = new Image();
	pic.src = _imageArray[_imageActive].src;

	if(!pic.complete)
	{
		pic.onload = function()
		{
			displayPicture(this);
		}
	}
	else
	{
		displayPicture(pic);
	}
}



/*
 ----------------------------------------------------------------------------
	displayPicture(img) :

		Shows the picture
 ----------------------------------------------------------------------------
 */

function displayPicture(img)
{
	resize(img.width,img.height);
	document.getElementById("z3nPhoto").src = img.src;
	document.getElementById("imgContainer").className = "show";
}



/*
 ----------------------------------------------------------------------------
	resize(w,h) :

		Resizes the picture, according to its width (w) and height (h)
 ----------------------------------------------------------------------------
 */

function resize(w,h)
{
	var maxH = parseInt(document.getElementById("overlay").offsetHeight)-20;
	var maxW = parseInt(document.getElementById("overlay").offsetWidth)-20;
	var new_h = h;
	var new_w = w;

	if(parseInt(w) > maxW || parseInt(h) > maxH)
	{
		if(parseInt(w)/maxW > parseInt(h)/maxH)
		{
			new_w = maxW;
			new_h = parseInt(new_w*h/w);
		}
		else
		{
			new_h = maxH;
			new_w = parseInt(new_h*w/h);
		}
	}

	var pw = parseInt((maxW-new_w)/2);
	var ph = parseInt((maxH-new_h)/2);

	document.getElementById("z3nPhoto").width = new_w;
	document.getElementById("z3nPhoto").height = new_h;

	document.getElementById("imgContainer").style.left = pw+"px";
	document.getElementById("imgContainer").style.top = ph+"px";
}



/*
 ----------------------------------------------------------------------------
	closePhoto() :

		Closes the div containing the picture
 ----------------------------------------------------------------------------
 */

function closePhoto()
{
	try
	{
		var i = document.getElementById("imgContainer");
		document.body.removeChild(i);
	}
	catch(e)
	{
	}
}



/*
 ----------------------------------------------------------------------------
	start() :

		Starts the diaporama
 ----------------------------------------------------------------------------
 */

function start()
{
	_diaporama = window.setInterval(next,parseInt(_delay));
	hideNavigationBar();
}



/*
 ----------------------------------------------------------------------------
	stop() :

		Stops the diaporama
 ----------------------------------------------------------------------------
 */

function stop()
{
	if(_diaporama != undefined)
	{
		window.clearInterval(_diaporama);
		_diaporama = null;

		document.getElementById("img_play").src = "_img/play.png";
	}
}



/*
 ----------------------------------------------------------------------------
	next() :

		Go to the next picture (called only with the diaporama mode)
 ----------------------------------------------------------------------------
 */

function next()
{
	updateVar(_imageActive+1);
	showPhoto(_imageActive);
}



/*
 ----------------------------------------------------------------------------
	end() :

		End the diaporama, closes everything
 ----------------------------------------------------------------------------
 */

function end()
{
	stop();
	closeNavigationBar();
	closePhoto();

	return false;
}





/*
 ****************************************************************************
 *	LET'S LAUNCH THE THING
 ****************************************************************************
 */


window.onload = function()
{
	if(document.getElementById("dir"))
	{
		closeLoading();
		zss_initialize();
	}
	else
	{
		closeLoading();
	}
}
