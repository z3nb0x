/*
 * *****************************************************************************
 *
 *
 *
 *
 *
 * *****************************************************************************
 */



// SETTINGS ____________________________________________________________________

// Interval of time between 2 pictures while in diaporama mode. In milliseconds.
var _delay = 3500;

// Margins :
var _marginTop = 10;
var _marginRight = _marginTop;
var _marginBottom = _marginTop;
var _marginLeft = _marginTop;



// SETTINGS : DO NOT EDIT ______________________________________________________

var _icones_path;
var _imageArray = new Array();
var _imageActive = 0;
var _diaporama = null;



// LAUNCH THE SCRIPT ___________________________________________________________

window.onload = function()
{
	initialize();
}



// FUNCTIONS ___________________________________________________________________

/*
 * initialize :
 *
 * 	Initialize the array of pictures
 *
 */
function initialize()
{
	_icones_path = "../../_themes/"+document.getElementById("theme").value+"/images/player/";

	if(document.getElementsByTagName)
	{
		var anchors = document.getElementsByTagName("a");
		var anchors_size = anchors.length;

		for(i=0,j=0 ; i<anchors_size ; i++)
		{
			var anchor = anchors[i];
			var relAttribute = String(anchor.getAttribute("rel"));

			if(anchor.getAttribute("href") && relAttribute.toLowerCase().match("zss"))
			{
				var img = new Image();
				img.src = String(anchor.getAttribute("href"));
				img.name = String(anchor.getAttribute("title"));

				_imageArray.push(img);

				anchor._index = j;
				anchor.onclick = function()
				{
					buildInterface();
					showPhoto(this._index);
					return false;
				}

				j++;
			}
		}
	}
}





/*
 * showPhoto :
 *
 * 	Load picture before showing it.
 *
 */
function showPhoto(index)
{
	// First, we have to update the different vars :
	updateVar(index);

	// Then, we can prepare the picture :
	var img = new Image();
	img.src = _imageArray[_imageActive].src;

	// If the picture wasn't loaded :
	if(!img.complete)
	{
		img.onload = function()
		{
			displayPicture(img);
		}
	}
	else
	{
		// Picture has already been loaded :
		displayPicture(img);
	}
}





/*
 * displayPicture :
 *
 * 	Resizes the picture and actually show it.
 *
 */
function displayPicture(img)
{
	var imgContainer = document.getElementById("imgContainer");
	imgContainer.style.display = "none";

	resize(img);

	imgContainer.style.display = "block";
}





/*
 * resize :
 *
 * 	Resizes the picture so that it fit the size of screen
 *
 */
function resize(img)
{
	var w = img.width;
	var h = img.height;

	//var b = document.getElementsByTagName("body").item(0);

	var maxW = parseInt(window.innerWidth - (_marginLeft + _marginRight));
	var maxH = parseInt(window.innerHeight- (_marginTop + _marginBottom));

	var new_w = w;
	var new_h = h;

	var _left = _marginLeft;
	var _top = _marginTop;

	if(parseInt(w) > maxW || parseInt(h) > maxH)
	{
		if(parseInt(w)/maxW > parseInt(h)/maxH)
		{
			new_w = maxW;
			new_h = parseInt(new_w*h/w);

			_top = parseInt((maxH-new_h)/2);
		}
		else
		{
			new_h = maxH;
			new_w = parseInt(new_h*w/h);

			_left = parseInt((maxW-new_w)/2);
		}
	}
	else
	{
		_left = parseInt((maxW-new_w)/2);
		_top = parseInt((maxH-new_h)/2);
	}


	// Let's resize and place the imgContainer <div> :
	var imgContainer = document.getElementById("imgContainer");
	imgContainer.style.left = _left+"px";
	imgContainer.style.top = _top+"px";
	imgContainer.style.width = new_w+"px";
	imgContainer.style.height = new_h+"px";
	imgContainer.style.zIndex = 1001;

	// Let's create the picture :
	var z3nPhoto = document.getElementById("z3nPhoto");
	z3nPhoto.src = img.src;
	z3nPhoto.style.width = new_w+"px";
	z3nPhoto.style.height = new_h+"px";
}





/*
 * makeInterface :
 *
 * 	Add the imgContainer <div> and z3nPhoto <img> to the body.
 * 	Also add the different buttons.
 *
 */
function buildInterface()
{
	showOverlay();

	var overlay = document.getElementById("overlay");
	overlay.onclick = function()
	{
		end();
	}

	try
	{
		var b = document.getElementsByTagName("body").item(0);

		// z3nPhoto : the picture
		var z3nPhoto = document.createElement("img");
		z3nPhoto.setAttribute("id","z3nPhoto");

		// ImgContainer : div that contains the picture :
		var imgContainer = document.createElement("div");
		imgContainer.setAttribute("id","imgContainer");
		imgContainer.style.width = "10px";
		imgContainer.style.height = "10px";
		imgContainer.style.display = "none";
		imgContainer.appendChild(z3nPhoto);


		// Start/pause diaporama button :
		var imgStart = document.createElement("img");
		imgStart.setAttribute("id","imgPlay");
		imgStart.setAttribute("alt","Start diaporama");
		imgStart.src = _icones_path + "play.png";

		var bStart = document.createElement("a");
		bStart.setAttribute("id","start");
		bStart.setAttribute("href","#");
		bStart.appendChild(imgStart);
		bStart.onclick = function()
		{
			start();
			return false;
		}

		// End Diaporama button :
		var imgEnd = document.createElement("img");
		imgEnd.setAttribute("id","imgClose");
		imgEnd.setAttribute("alt","End diaporama");
		imgEnd.src = _icones_path + "stop.png";

		var bEnd = document.createElement("a");
		bEnd.setAttribute("id","close");
		bEnd.setAttribute("href","#");
		bEnd.appendChild(imgEnd);
		bEnd.onclick = function()
		{
			end();
			return false;
		}

		// Next pic button :
		var imgNext = document.createElement("img");
		imgNext.setAttribute("id","imgNext");
		imgNext.setAttribute("alt","Next picture");
		imgNext.src = _icones_path + "next.png";

		var bNext = document.createElement("a");
		bNext.setAttribute("id","next");
		bNext.setAttribute("href","#");
		bNext.appendChild(imgNext);
		bNext.onclick = function()
		{
			next();
			return false;
		}

		// Previous pic button :
		var imgPrev = document.createElement("img");
		imgPrev.setAttribute("id","imgPrev");
		imgPrev.setAttribute("alt","Previous picture");
		imgPrev.src = _icones_path + "prev.png";

		var bPrev = document.createElement("a");
		bPrev.setAttribute("id","prev");
		bPrev.setAttribute("href","#");
		bPrev.appendChild(imgPrev);
		bPrev.onclick = function()
		{
			prev();
			return false;
		}

		// Navigation bar :
		var navBar = document.createElement("div");
		navBar.setAttribute("id","navigation_bar");
		navBar.appendChild(bPrev);
		navBar.appendChild(bStart);
		navBar.appendChild(bEnd);
		navBar.appendChild(bNext);


		// Add the navigation bar and containers to the body :
		b.appendChild(imgContainer);
		b.appendChild(navBar);
	}
	catch(e)
	{
		// Ignore Exceptions...
	}
}





/*
 * destroyInterface :
 *
 * 	Removes the imgContainer <div> and the z3nPhoto <img> from body.
 *
 */
function destroyInterface()
{
	try
	{
		var imgContainer = document.getElementById("imgContainer");
		document.body.removeChild(imgContainer);

		var navBar = document.getElementById("navigation_bar");
		document.body.removeChild(navBar);
	}
	catch(e)
	{
		// Ignore Exceptions...
	}
}





/*
 * updateVar :
 *
 * 	Updates the variables
 *
 */
function updateVar(index)
{
	if(index<0)
		index = _imageArray.length-1;

	if(index>_imageArray.length-1)
		index = 0;

	_imageActive = index;
}





/*
 * start :
 *
 * 	Starts the diaporama mode
 *
 */
function start()
{
	_diaporama = window.setInterval(next, parseInt(_delay));

	document.getElementById("imgPlay").src = _icones_path+"pause.png";
	document.getElementById("start").onclick = function()
	{
		stop();
	}
}





/*
 * stop :
 *
 * 	Stops the diaporama
 *
 */
function stop()
{
	if(_diaporama != undefined)
	{
		window.clearInterval(_diaporama);
		_diaporama = null;

		document.getElementById("imgPlay").src = _icones_path+"play.png";
		document.getElementById("start").onclick = function()
		{
			start();
		}
	}
}





/*
 * next :
 *
 * 	Go to the next picture (or the previous one...)
 *
 */
function next()
{
	updateVar(_imageActive+1);
	showPhoto(_imageActive);
}





/*
 * end :
 *
 * 	Ends the diaporama, returns to gallery
 *
 */
function end()
{
	stop();
	destroyInterface();
	closeOverlay();
}