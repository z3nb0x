<?php
	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Image.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");


	if(isset($_GET["img"]) && !empty($_GET["img"]))
	{
		$img = urldecode($_GET["img"]);
		$img_sum = "photo_".md5($img).".png";
		$img_path = $Z3NB0X_CONFIG["CACHE_PATH"]."/".$img_sum;

		if(file_exists($img_path) && filemtime($img_path) >= filemtime($img))
		{
			// We use the cached image

			header("Content-type: image/png");
			readfile($img_path);
			exit();
		}
		else
		{
			// We build a new image

			$pic = new Image($img);
			$pic->resize($MODULE_CONFIG["MAX_HEIGHT"], $MODULE_CONFIG["MAX_WIDTH"]);

			$pic->save("PNG",$img_path);
			//chown($cover_path,"z3nb0x");
			//chgrp($img_path,"z3nb0x");
			chmod($img_path,0755);
			$pic->draw("PNG");
		}
	}
	else
	{
		exit();
	}
?>