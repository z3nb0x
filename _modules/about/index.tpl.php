<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $this->getName(); ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $this->getTheme(); ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body class="opaque">
	<h1><?php echo $this->getName(); ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $this->getPicture(); ?>" />

	<div id="main">
		<p class="preamble">
			<?php echo $this->getPreamble(); ?>
		</p>

		<h2><?php echo $this->getLicenceTxt(); ?></h2>
		<p>
			z3nb0x is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
		</p>
		<p>
			z3nb0x is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
			See the GNU General Public License for more details.
		</p>
		<p>
			You should have received a copy of the GNU General Public License along with z3nb0x; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
    		</p>

		<h2><?php echo $this->getCopyrightTxt(); ?></h2>
		<p>
			<?php echo $this->getCopyright(); ?>
		</p>

		<h2><?php echo $this->getMoreTxt(); ?></h2>
		<p>
			<?php echo $this->getMore(); ?>
		</p>

		<p class="centered">
			Copyright &copy; François KUBLER - 2007-2008
		</p>
	</div>

	<a id="back" href="<?php echo $this->getBacklink(); ?>">&lt;</a>
</body>
</html>