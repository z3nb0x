<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x - Ze ENhanced b0x ! __P__ Update</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1>{PLUGIN_TITLE}</h1>
	<img class="plugin_img" alt="" src="{PLUGIN_IMG}" />

	<div id="main">
		<!-- BEGIN plugin -->
		<dl>
			<dt>{NAME}</dt>
			<dd>{COPYRIGHT}</dd>
			<dd><span class="smaller">{INSTALLED_VERSION} :</span> {IV}</dd>
			<dd><span class="smaller">{LAST_VERSION} :</span> {LV}</dd>
			<dd class="desc">{DESCRIPTION}</dd>
		</dl>
		<!-- END plugin -->
	</div>

	<a id="back" href="../../menu_system.xml">&lt;</a>
</body>
</html>