<?php
	$_UPDATE__PLUGIN_IMG = "plugin.png";			// Path of plugin image

	$_UPDATE__PLUGIN_MESSAGES = array(
		"en" => array(
			"INSTALLED_VERSION" => "Installed version",
			"LAST_VERSION" => "Last available version",
			"BY" => "By",
			"NO_UPDATE" => "No update available",
		),
		"fr" => array(
			"INSTALLED_VERSION" => "Version installée",
			"LAST_VERSION" => "Dernière version disponible",
			"BY" => "Par",
			"NO_UPDATE" => "Aucune mise à jour n'est disponible",
		),
	);

	$_UPDATE__PLUGINS_ERR = array(
		"NO_PLUGIN_DIR" => array(
			"en" => array(
				"MSG" => "No plugin !",
				"EXPLANATION" => "I wasn't able to find a valid plugin directory.<br />Please check your <i>config.inc.php</i> ;)",
			),
			"fr" => array(
				"MSG" => "Aucun plugin trouvé !",
				"EXPLANATION" => "Le système n'a trouvé aucun plugin...ce qui est tout à fait anormal !<br />Vérifiez le fichier <i>config.inc.php</i> du plugin ;)",
			),
		),

		"NO_SOURCE" => array(
			"en" => array(
				"MSG" => "No available sources",
				"EXPLANATION" => "The updates sources aren't available.<br />Please check your <i>config.inc.php</i> ;)<br />If the file mentions a internet source of updates, please check your connexion.",
			),
			"fr" => array(
				"MSG" => "Sources indisponibles",
				"EXPLANATION" => "Les sources de mises à jour sont indisponibles.<br />Vérifiez le fichier <i>config.inc.php</i> du plugin ;)<br />Si la configuration fait référence à une source sur Internet, assurez-vous d'être bien connecté à Internet !",
			),
		),
	);
?>