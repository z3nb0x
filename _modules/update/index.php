﻿<?php
	// REQUIRE

	require_once(dirname(__FILE__)."/../../_inc/template.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");

	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");
	require_once(dirname(__FILE__)."/../../_inc/Plugin.class.php");

	require_once(dirname(__FILE__)."/config.inc.php");



	// i18n

	bindtextdomain("update",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("update","UTF-8");



	// TEMPLATE

	$template = new Template(".");
	$template->set_file("main","index.tpl");
	$template->set_block("main","err","error");

	$template->set_var(array(
		"THEME" => $_Z3N__THEME,
		"PLUGIN_TITLE" => dgettext("update","Update"),
		"PLUGIN_IMG" => $_UPDATE__PLUGIN_IMG,
	));



	// TRAITEMENT

	// Vérification du path des plugins
	if(!is_dir($_Z3N__PLUGINS_PATH))
	{
		$template->set_var(array(
			"ERROR" => dgettext("update","No plugin found !"),
			"EXPLANATION" => sprintf(dgettext("update","Woups, I wasn't able to find your plugins directory, which is very, VERY, weird !<br />Please note that they <strong>have to</strong> be stored in <i>%s</i>."),$_Z3N__PLUGINS_PATH),
		));
		$template->parse("error","err",true);
	}
	else
	{
		// Vérification du path des sources
		if(!is_dir($_Z3N__UPDATES_PATH))
		{
			$template->set_var(array(
				"ERROR" => dgettext("update","No update source found !"),
				"EXPLANATION" => sprintf(dgettext("update","I wasn't able to find updates. Please check if <i>%s</i> is a real source for z3nb0x updates."),$_Z3N__UPDATES_PATH),
			));
			$template->parse("error","err",true);
		}
		else
		{
			// CHECK OUT z3nb0x UPDATES :
			include "update_z3nb0x.php";

			// CHECK OUT INSTALLED PLUGINS UPDATES :
			include "update_plugins.php";
		}
	}



	// AFFICHAGE

	$template->pparse("out","main");
?>