<?php
	/*
	 * ***********************************************************
	 *
	 *  Checks if plugins are up-to-date or not.
	 *
	 * ***********************************************************
	 */

	// We get the list of installed plugins :
	$b = new Browser($_Z3N__PLUGINS_PATH);
	$b->hide_hidden_dirs();
	$b->order_dirs("ASC");
	$plugins = $b->getDirs();

	// Check out the versions :
	$nb_maj = 0;
	foreach($plugins as $plug)
	{
		$xml1 = $_Z3N__PLUGINS_PATH."/".$plug."/INFO.xml";
		$xml2 = $_Z3N__UPDATES_PATH."/".$plug."/INFO.xml";

		$p1 = @Plugin::read_file($xml1);
		$p2 = @Plugin::read_file($xml2);

		if(!is_null($p1) && !is_null($p2))
		{
			if($p2->is_this_a_new_version($p1->getVersion()))
			{
				$nb_maj++;
			}

		}
	}

	if($nb_maj == 1)
	{
		$plug_update = dgettext("update","There is 1 update available !");
		$c_plug_update = "nok";
	}
	else
	{
		if($nb_maj > 1)
		{
			$plug_update = sprintf(dgettext("update","There are %s updates available !"),$nb_maj);
			$c_plug_update = "nok";
		}
		else
		{
			$plug_update = dgettext("update","Your plugins are up-to-date !");
			$c_plug_update = "ok";
		}
	}


	// Filling the template :

	$template->set_var(array(
		"T_PLUG_UPDATE" => dgettext("update","Plugins"),
		"PLUG_UPDATE" => $plug_update,
		"C_PLUG_UPDATE" => $c_plug_update,
	));
?>