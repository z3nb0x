<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:update]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/{THEME}/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
	<h1>{PLUGIN_TITLE}</h1>
	<img class="plugin_img" alt="" src="{PLUGIN_IMG}" />

	<div id="main">
		<div class="update">
			<h2>{T_Z3N_UPDATE}</h2>
			<p class="{C_Z3N_UPDATE}">
				{Z3N_UPDATE}
			</p>
		</div>

		<div class="update">
			<h2>{T_PLUG_UPDATE}</h2>
			<p class="{C_PLUG_UPDATE}">
				{PLUG_UPDATE}
			</p>
		</div>

		<div class="update">
			<h2>{T_THEMES_UPDATE}</h2>
			<p>
				{THEMES_UPDATE}
			</p>
		</div>

		<!-- BEGIN err -->
		<div class="error">
			<p class="msg">
				{ERROR}
			</p>
			<p class="explanation">
				{EXPLANATION}
			</p>
		</div>
		<!-- END err -->
	</div>

	<a id="back" href="../../_menus/menu.php?menu=system">&lt;</a>
</body>
</html>