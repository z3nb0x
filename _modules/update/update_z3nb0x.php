<?php
	/*
	 * ***********************************************************
	 *
	 *  Checks if plugins are up-to-date or not.
	 *
	 * ***********************************************************
	 */

	$ret = NULL;

	$xml1 = new DomDocument();
	$ok1 = $xml1->load($_Z3N__UPDATES_PATH."/INFO.xml");

	$xml2 = new DomDocument();
	$ok2 = $xml2->load($_Z3N__INSTALL_PATH."/INFO.xml");

	if(!$ok1 || !$ok2)
	{
		//trigger_error("<br /> _ Plugin error : [".__METHOD__."] Le fichier spécifié n'existe pas !");
	}
	else
	{
		$flux1 = $xml1->getElementsByTagName("z3nb0x");
		$flux2 = $xml2->getElementsByTagName("z3nb0x");

		$pl1 = $flux1->item(0);
		$pl2 = $flux2->item(0);

		$v1 = "N/A";
		$v2 = "N/A";

		foreach($pl1->childNodes as $node)
		{
			switch($node->nodeName)
			{
				case "version":
					$v1 = $node->nodeValue;
					break;
			}
		}

		foreach($pl2->childNodes as $node)
		{
			switch($node->nodeName)
			{
				case "version":
					$v2 = $node->nodeValue;
					break;
			}
		}

		if($v1 == "N/A" || $v2 == "N/A")
		{
			//
		}
		else
		{
			if(version_compare($v1,$v2,">") > 0)
			{
				$z3n_update = dgettext("update","There is an update for z3nb0x !");
				$c_z3n_update = "nok";
			}
			else
			{
				$z3n_update = dgettext("update","z3nb0x is up-to-date !");
				$c_z3n_update = "ok";
			}
		}


		// Filling the template :

		$template->set_var(array(
			"T_Z3N_UPDATE" => "z3nb0x",
			"Z3N_UPDATE" => $z3n_update,
			"C_Z3N_UPDATE" => $c_z3n_update,
		));
	}
?>