﻿<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("shutdown",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("shutdown","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("shutdown", "Shutdown"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);

	$_DATA["QUESTION"] = dgettext("shutdown", "Do you really want to quit ?");
	$_DATA["YES"] = dgettext("shutdown", "Yes, quit");
	$_DATA["NO"] = dgettext("shutdown", "No, I stay");





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	include(dirname(__FILE__)."/index.tpl.php");

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>