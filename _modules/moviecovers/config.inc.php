﻿;
; movieCoversSearch module ini file.
;
;	Edit with care.
;

; Path of the plugin picture :
IMG = "plugin.png"

; URL of reference file :
REF = "http://www.moviecovers.com/DATA/movies.txt"


<?php
	$_MOVIECOVERSSEARCH__PLUGIN_IMG = "plugin.png";						// Path de l'image du plugin
	$_MOVIECOVERSSEARCH__REFERENTIEL = "http://www.moviecovers.com/DATA/movies.txt";	// Path du fichier contenant la liste des films dispos sur le site moviecovers.com


	// Error messages :

	$_MOVIECOVERSSEARCH__ERR = array(
		"NO_TITLE" => array( // Message displayed when the browsed dir is empty
			"fr" => array(
				"MSG" => "Chaine de recherche vide !",
				"EXPLANATION" => "Vous êtes en train de ne...rien...rechercher !"
			),
		),

		"NO_RESULT" => array( // Message displayed when the search returns 0 result
			"fr" => array(
				"MSG" => "Aucun résultat !",
				"EXPLANATION" => "Nous n'avons trouv&eacute; aucun r&eacute;sultat, essayez peut-&ecirc;tre de renommer le fichier diff&eacute;remment ;)"
			),
		),

		"NO_INTERNET" => array( // Message displayed when we can't fetch infos
			"fr" => array(
				"MSG" => "Impossible de r&eacute;cup&eacute;rer les infos !",
				"EXPLANATION" => "Je n'ai malheureusement pas pu r&eacute;cup&eacute;rer les informations depuis <i>MovieCovers.com</i> !<br />Etes-vous bien s&ucirc;r d'&ecirc;tre connect&eacute; &agrave; Internet ?",
			),
		),

		"SAVE_INFOS" => array( // Message displayed when unable to save infos
			"fr" => array(
				"MSG" => "Impossible de sauvegarder les infos !",
				"EXPLANATION" => "Je n'ai malheureusement pas pu sauvegarder les informations sur le disque dur. Etes-vous s&ucirc;rs que j'ai le droit d'&eacute;crire sur le disque ?",
			),
		),

		"SAVE_COVER" => array( // Message displayed when unable to save the cover
			"fr" => array(
				"MSG" => "Impossible de sauvegarder l'affiche !",
				"EXPLANATION" => "Je n'ai malheureusement pas pu sauvegarder l'affiche sur le disque dur. Etes-vous s&ucirc;rs que j'ai le droit d'&eacute;crire sur le disque ?",
			),
		),

		"SAVE_INFOS_OK" => array( // Message displayed when saving info was SUCCESSFUL
			"fr" => array(
				"MSG" => "Enregistrement des infos",
				"EXPLANATION" => "L'enregistrement s'est bien deroule !",
			),
		),
	);
?>
