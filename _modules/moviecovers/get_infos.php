<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/MovieCoversSearch.class.php");
	require_once(dirname(__FILE__)."/MovieCoversFilm.class.php");

	$MODULE_MOVIES_CONFIG = parse_ini_file(dirname(__FILE__)."/../movies/MODULE.ini");
	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("moviecovers",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("moviecovers","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("moviecovers", "MovieCovers.com search"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = $_SERVER["HTTP_REFERER"];


	// OK, now, the main part

	$res = false;

	if(!empty($_GET["idmc"]) && !empty($_GET["movie"]))
	{
		$idmc = $_GET["idmc"];
		$mov = urldecode($_GET["movie"]);
		$movie = basename($mov);

		$infos_p = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_MOVIES_CONFIG["INFOS_PATH"]."/";
		$cover_p = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_MOVIES_CONFIG["COVERS_PATH"]."/";

		$pathinf = $infos_p.$movie.".txt";
		$pathimg = $cover_p.$movie.".jpg";

		$inf = MovieCoversSearch::get_infos($idmc);

		$saveinf = MovieCoversFilm::save_infos($pathinf, $inf);
		$saveimg = MovieCoversFilm::save_cover($pathimg, $idmc);

		if($saveinf!==false && $saveimg!==false)
		{
			$_DATA["MOVIE"] = array(
				"TITLE" => dgettext("moviecovers","Save info"),
				"EXPLANATION" => dgettext("moviecovers","Info were saved properly, enjoy !"),
			);
			$_DATA["BACK_LINK"] = "../movies/infos.php?movie=".urlencode($mov);
		}
		else
		{
			if($saveinf===false)
			{
				$_DATA["ERR"] = array(
					"ERROR" => dgettext("moviecovers","Unable to save info !"),
					"EXPLANATION" => dgettext("moviecovers","I'm sorry, I wasn't able to save the information on the local hard disk drive. Please be sure that I'm allowed to write on disk :)"),
				);
			}
			else
			{
				$_DATA["ERR"] = array(
					"ERROR" => dgettext("moviecovers","Unable to save cover !"),
					"EXPLANATION" => dgettext("moviecovers","I'm sorry, I wasn't able to save the cover on the local hard disk drive. Please be sure that I'm allowed to write on disk :)"),
				);
			}
		}
	}
	else
	{
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("moviecovers","No file specified in request !"),
			"EXPLANATION" => dgettext("moviecovers","You are looking for...nothing..."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/get_infos.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>