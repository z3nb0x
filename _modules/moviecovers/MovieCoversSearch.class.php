﻿<?php
	class MovieCoversSearch
	{
		// CONSTANTES

		const MOTIF_ARTICLE_DEVANT = "#(LES|THE|UNE|C7A|CCM|CDM|CIM|VRM|EL'|L'|LE|LA|UN|AN|EL|L){1}\ (.*)#";		// Expression régulière permettant de savoir si la chaîne recherchée contient un article au début.
		const MOTIF_ARTICLE_FIN = "#(.*)\ \((LES|THE|UNE|C7A|CCM|CDM|CIM|VRM|EL'|L'|LE|LA|UN|AN|EL|L){1}\)#";		// Expression régulière permettant de savoir si la chaîne recherchée contient un article entre parenthèses en fin de chaîne.
		const URL = "http://www.moviecovers.com/getfilm.html";								// URL du script permettant de récupérer les infos sur les films.



		// ATTRIBUTS

		private $referentiel;												// Fichier contenant la liste des films.
		private $search;												// Le nom du film que l'on cherche.



		// CONSTRUCTEUR

		public function __construct($s,$ref)
		{
			$this->_set__referentiel($ref);
			$this->_set__search($s);
		}



		// ACCESSEURS

		public function _set__referentiel($ref="http://www.moviecovers.com/DATA/movies.txt")
		{
			$this->referentiel = $ref;
		}

		public function _set__search($s)
		{
			$this->search = $s;
		}

		public function _get__referentiel()
		{
			return $this->referentiel;
		}

		public function _get__search()
		{
			return $this->search;
		}



		// METHODES

		public function search()
		{
			$res = false;			// Tableau contenant les résultats ou FALSE si aucun résultat

			// On formate la chaine à rechercher :
			$s = $this->format();

			// On supprime les articles en début de chaine :
			$s = self::suppr_articles($s);

			// On crée le motif à rechercher :
			$motif = "#".$s."#";

			// On recherche :
			$films = file($this->_get__referentiel(), FILE_IGNORE_NEW_LINES);

			if($films!==false)
			{
				$r = array();
				$r = preg_grep($motif,$films);

				$res = array();
				foreach($r as $wrong_IDMC)
				{
					$wrong_IDMC = rtrim($wrong_IDMC);

					$article_en_fin = preg_match(self::MOTIF_ARTICLE_FIN, $wrong_IDMC, $matches);

					if($article_en_fin>0)
					{
						$corps = $matches[1];
						$article = $matches[2];

						$good_IDMC = $article." ".$corps;

						$res[$wrong_IDMC] = $good_IDMC;
					}
					else
					{
						$res[$wrong_IDMC]= rtrim($wrong_IDMC);
					}
				}
			}

			return $res;
		}



		private function format()
		{
			$lookfor = array("é","è","ê","ë","à","@","ï","î","ô","ö","ñ",":",",");
			$replacewith = array("e","e","e","e","a","a","i","i","o","o","n","-","");

			$s = str_replace($lookfor,$replacewith,$this->_get__search());
			$s = strtoupper($s);

			return $s;
		}


		private static function suppr_articles($s)
		{
			$article_au_debut = preg_match(self::MOTIF_ARTICLE_DEVANT,$s,$matches);

			if($article_au_debut>0)
			{
				$taille_article = strlen($matches[1]);

				$s = substr($s,$taille_article+1);
			}

			return $s;
		}


		public static function get_infos($IDMC)
		{
			$res = FALSE;			// Chaine contenant les infos ou FALSE en cas d'erreur


			// On parse l'URL du script :

			$_url = parse_url(self::URL);


			// On prépare les données à envoyer :

			$ar = array("idmc" => stripslashes($IDMC));
			$args = http_build_query($ar);


			// On ouvre la connexion :

			$socket = @fsockopen($_url["host"],80,$errno,$errstr);

			if($socket)
			{
				$size = strlen($args);

				$request = "POST ".$_url["path"]." HTTP/1.1\n";
				$request .= "Host: ".$_url["host"]."\n";
				$request .= "Connection: Close\r\n";
				$request .= "Content-type: application/x-www-form-urlencoded\n";
				$request .= "Content-length: ".$size."\n\n";
				$request .= $args."\n";

				$fput = fputs($socket, $request);

				$response="";

				while(!feof($socket))
				{
					$response .= fread($socket,4056);
				}

				$res = split("\r\n\r\n",$response);
				$res = $res[1];

				@fclose($socket);
			}

			return $res;
		}
	}
?>