<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");
	require_once(dirname(__FILE__)."/MovieCoversSearch.class.php");
	require_once(dirname(__FILE__)."/MovieCoversFilm.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("moviecovers",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("moviecovers","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("moviecovers", "MovieCovers.com search"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "../movies/index.php";


	// OK, now, the main part

	$res = false;

	if(isset($_GET["movie"]) && !empty($_GET["movie"]))
	{
		$_DATA["BACK_LINK"] = $_SERVER["HTTP_REFERER"];

		$mov = urldecode($_GET["movie"]);
		$fname = basename($mov);
		$s = basename(($fname),".".Browser::get_extension($fname));


		// Let's see if the reference file is ok :

		$ref = @fopen($MODULE_CONFIG["REF"],"r");

		if($ref !== false)
		{
			// OK, the reference is available
			@fclose($ref);

			// Let's create a new research :
			$mcs = new MovieCoversSearch($s, $MODULE_CONFIG["REF"]);
			$r = $mcs->search();

			// Let's see if we have results...
			if($r !== false && count($r)>0)
			{
				$_DATA["TXT"]["REAL"] = dgettext("moviecovers","Director(s)");
				$_DATA["TXT"]["ACT"] = dgettext("moviecovers","Actors");

				$_DATA["RESULTS"] = array();

				foreach($r as $key=>$idmc)
				{
					$inf = MovieCoversSearch::get_infos($idmc);

					if($inf !== false)
					{
						$f = new MovieCoversFilm($idmc, $key, $inf);

						$arr = array(
							"IMG" => $f->get_small_picture(),
							"TITLE" => $f->_get__titre(),
							"YEAR" => $f->_get__annee(),
							"REAL" => $f->get_realisateurs_as_string(),
							"ACT" => $f->get_acteurs_as_string(),
						);

						$arr = array_map("htmlentities", $arr);
						$arr["IDMC"] = urlencode($f->_get__IDMC());
						$arr["MOV"] = urlencode($mov);

						$_DATA["RESULTS"][] = $arr;
					}
				}
			}
			else
			{
				// There was no result :
				$_DATA["ERR"] = array(
					"ERROR" => dgettext("moviecovers", "Sorry, no result ! "),
					"EXPLANATION" => dgettext("moviecovers", "I wasn't able to find any movie that matches the name of your file. Renaming the file might get more results."),
				);
			}
		}
		else
		{
			// Reference file is not available :
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("moviecovers", "Unable to retrieve information !"),
				"EXPLANATION" => sprintf(dgettext("moviecovers", "I'm sorry, I wasn't able to read information from <i>%s</i> !<br />Please ensure that this file really exists."),$_MOVIECOVERSSEARCH__REFERENTIEL),
			);
		}
	}
	else
	{
		// Nothing was specified in request :
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("moviecovers","No file specified in request !"),
			"EXPLANATION" => dgettext("moviecovers","You are looking for...nothing..."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/index.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>