﻿<?php
	class MovieCoversFilm
	{
		// CONSTANTES

		//const IMG_URL = "http://data.moviecovers.com/DATA/zipcache/@IDMC@.jpg";						// URL du script permettant de récupérer les images en taille normal.
		const IMG_URL = "http://www.moviecovers.com/getjpg.html/@IDMC@.jpg";
		const IMGMINI_URL = "http://www.moviecovers.com/DATA/thumbs/films-@PREMIERE_LETTRE@/@IDMC@.jpg";		// URL du script permettant de récupérer les images en taille miniature.



		// ATTRIBUTS

		private $widmc;
		private $idmc;
		private $titre;
		private $annee;
		private $duree;
		private $nationalites;
		private $genres;
		private $realisateurs;
		private $acteurs;
		private $synopsis;



		// CONSTRUCTEUR

		public function __construct($IDMC,$wrong_IDMC,$infos)
		{
			$vars = split("\r\n",$infos);

			$this->_set__wrong_IDMC($wrong_IDMC);
			$this->_set__IDMC($IDMC);
			$this->_set__titre($vars[0]);
			$this->_set__realisateurs($vars[1]);
			$this->_set__annee($vars[2]);
			$this->_set__nationalites($vars[3]);
			$this->_set__genres($vars[4]);
			$this->_set__duree($vars[5]);
			$this->_set__acteurs($vars[6]);
			$this->_set__synopsis($vars[7]);
		}



		// ACCESSEURS

		public function _set__wrong_IDMC($w)
		{
			$this->widmc = $w;
		}

		public function _set__IDMC($i)
		{
			$this->idmc = $i;
		}

		public function _set__titre($t)
		{
			$this->titre = $t;
		}

		public function _set__realisateurs($r)
		{
			$realisateurs = array();
			$real = split("/",$r);

			foreach($real as $realisateur)
			{
				$realisateurs[] = $realisateur;
			}

			$this->realisateurs = $realisateurs;
		}

		public function _set__annee($a)
		{
			$this->annee = $a;
		}

		public function _set__nationalites($n)
		{
			$nationalites = array();

			$nat = split("/",$n);

			foreach($nat as $nationalite)
			{
				$nationalites[] = $nationalite;
			}

			$this->nationalites = $nationalites;
		}

		public function _set__genres($g)
		{
			$genres = array();

			$gen = split(",",$g);

			foreach($gen as $genre)
			{
				$genres[] = $genre;
			}

			$this->genres = $genres;
		}

		public function _set__duree($d)
		{
			$this->duree = $d;
		}

		public function _set__acteurs($a)
		{
			$acteurs = array();

			$act = split(";",$a);

			foreach($act as $acteur)
			{
				$acteurs[] = $acteur;
			}

			$this->acteurs = $acteurs;
		}

		public function _set__synopsis($s)
		{
			$this->synopsis = $s;
		}


		// ---


		public function _get__wrong_IDMC()
		{
			return $this->widmc;
		}

		public function _get__IDMC()
		{
			return $this->idmc;
		}

		public function _get__titre()
		{
			return $this->titre;
		}

		public function _get__realisateurs()
		{
			return $this->realisateurs;
		}

		public function _get__annee()
		{
			return $this->annee;
		}

		public function _get__nationalites()
		{
			return $this->nationalites;
		}

		public function _get__genres()
		{
			return $this->genres;
		}

		public function _get__duree()
		{
			return $this->duree;
		}

		public function _get__acteurs()
		{
			return $this->acteurs;
		}

		public function _get__synopsis()
		{
			return $this->synopsis;
		}



		// METHODES

		public static function save_picture($filename)
		{

		}

		public function get_small_infos()
		{
			$widmc = $this->_get__wrong_IDMC();
			$idmc = $this->_get__idmc();
			$idmc = str_replace(":","-",$idmc);

			$img = self::IMGMINI_URL;
			$img = str_replace("@PREMIERE_LETTRE@",strtolower($widmc{0}),$img);
			$img = str_replace("@IDMC@",$idmc,$img);

			$res = "<film>\r\n";
			$res .= "\t<titre>".$this->_get__titre()."</titre>\r\n";
			$res .= "\t<annee>".$this->_get__annee()."</annee>\r\n";
			$res .= "\t<cover>".$img."</cover>\r\n";
			$res .= "</film>";

			return $res;
		}

		public function get_small_picture()
		{
			$widmc = $this->_get__wrong_IDMC();
			$idmc = $this->_get__idmc();
			$idmc = str_replace(":","-",$idmc);

			$img = self::IMGMINI_URL;
			$img = str_replace("@PREMIERE_LETTRE@",strtolower($widmc{0}),$img);
			$img = str_replace("@IDMC@",$idmc,$img);

			return $img;
		}

		public static function get_big_picture($idmc)
		{
			$img = self::IMG_URL;
			$img = str_replace("@IDMC@",$idmc,$img);

			$res = "<img src=\"".$img."\" />";

			return $res;
		}

		public function get_realisateurs_as_string()
		{
			$res = "";

			foreach($this->_get__realisateurs() as $r)
			{
				$res .= $r;
				$res .= ", ";
			}

			$res = substr($res,0,strlen($res)-2);

			return $res;
		}

		public function get_acteurs_as_string()
		{
			$res = "";

			foreach($this->_get__acteurs() as $a)
			{
				$res .= $a;
				$res .= ", ";
			}

			$res = substr($res,0,strlen($res)-2);

			return $res;
		}

		public function get_nationalites_as_string()
		{
			$res = "";

			foreach($this->_get__nationalites() as $n)
			{
				$res .= $n;
				$res .= ", ";
			}

			$res = substr($res,0,strlen($res)-2);

			return $res;
		}

		public function get_genres_as_string()
		{
			$res = "";

			foreach($this->_get__genres() as $g)
			{
				$res .= $g;
				$res .= ", ";
			}

			$res = substr($res,0,strlen($res)-2);

			return $res;
		}

		public static function save_infos($filename,$infos)
		{
			$res = FALSE;

			$fp = @fopen($filename,"w");

			if($fp)
			{
				$res = @fwrite($fp,$infos);

				if($res !== FALSE)
				{
					$res = TRUE;
				}

				@fclose($fp);
			}

			return $res;
		}

		public static function save_cover($filename,$idmc)
		{
			$res = FALSE;

			$img = self::IMG_URL;
			$img = str_replace("@IDMC@",rawurlencode(stripslashes($idmc)),$img);
			//$img = str_replace("@IDMC@",$idmc,$img);

			$i = @imagecreatefromjpeg("$img");

			if($i!==FALSE)
				$res = @imagejpeg($i,$filename);

			return $res;
		}
	}
?>