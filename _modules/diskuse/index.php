<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Browser.class.php");
	require_once(dirname(__FILE__)."/Stats_Camembert.php");
	require_once(dirname(__FILE__)."/diskuse_utils.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");
	$colors = explode("|", $MODULE_CONFIG["COLORS"]);





	// i18n ________________________________________________________________

	bindtextdomain("diskuse",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("diskuse","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("diskuse", "Disk Usage"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "../../_menus/menu.php?menu=informations";


	// And finally, the main part

	$stats = array();
	$i = 0;

	// We use the Browser class to get the list of dirs :

	try
	{
		$b = new Browser($Z3NB0X_CONFIG["MEDIA_PATH"]);

		$b->delete_dir("lost+found");
		$b->hide_hidden_dirs();

		$dirs = $b->getDirs();

		// We build an array which is going to be needed to build the pie chart :

		foreach($dirs as $d)
		{
			$s = array(
				"name" => htmlentities(ucfirst($d)),
				"val" => dir_size($Z3NB0X_CONFIG["MEDIA_PATH"]."/".$d),
				"color" => $colors[$i],
			);
			$stats[] = $s;
			$i++;
		}

		// We sort the stats array :

		foreach($stats as $key => $row)
		{
			$val[$key]  = $row["val"];
		}

		array_multisort($val, SORT_ASC, $stats);

		// We had the free space at the end :

		$free_space = disk_free_space($Z3NB0X_CONFIG["MEDIA_PATH"]);
		$stats[] = array(
			"name" => htmlentities(dgettext("diskuse","Free space")),
			"val" => $free_space,
			"color" => $colors[$i++],
		);

		// Then we create the pie chart :

		$cam = new Stats_Camembert($stats, $MODULE_CONFIG["WIDTH"], $MODULE_CONFIG["HEIGHT"], $MODULE_CONFIG["3D"]);
		$cam->save_as($Z3NB0X_CONFIG["CACHE_PATH"]."/diskuse.png");



		$_DATA["STATS_IMG"] = htmlentities($Z3NB0X_CONFIG["URL"]."/".basename($Z3NB0X_CONFIG["CACHE_PATH"])."/diskuse.png");
		$_DATA["LEGEND"] = array();

		foreach($stats as $line)
		{
			$_DATA["LEGEND"][] = array(
				"COLOR" => $line["color"],
				"NAME" => $line["name"],
				"VALUE" => get_size_in_best_unit($line["val"]),
			);
		}
	}
	catch(Exception $e)
	{
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("diskuse","This directory doesn't exist !"),
			"EXPLANATION" => dgettext("diskuse","Please ensure that your <i>MODULE.ini</i> configuration file points to a directory that really exist :)"),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/index.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>