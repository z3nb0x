<?php
/*
 * ----------------------------
 *
 * class Stats_Camembert [part of z3nb0x]
 *
 * - - - - - -
 *
 *	This PHP class allows you to create a graphic pie
 *
 * ----------------------------
 */

	class Stats_Camembert
	{
	// +   A T T R I B U T E S

		const DARKNESS = "50";						// Color for the 3D effect

		private $image;							// Image
		private $stats;							// Array containing the stats
		private $three_d;						// Height of 3D effect
		private $x;							// Width of picture
		private $y;							// Height of picture


	// +   C O N S T R U C T O R

		public function __construct($_stats, $_x, $_y, $_three_d)
		{
			$this->_set__stats($_stats);
			$this->_set__dimensions($_x,$_y);
			$this->_set__three_d($_three_d);
			$this->image = imagecreatetruecolor($this->_get__x(),$this->_get__y());
			$this->build();
		}


	// +   A C C E S S O R S

		public function _set__stats($_stats)
		{
			$this->stats = $_stats;
		}

		public function _set__dimensions($_x,$_y)
		{
			$this->x = $_x;
			$this->y = $_y;
		}
		
		public function _set__three_d($_three_d)
		{
			$this->three_d = $_three_d;
		}


		public function _get__stats()
		{
			return $this->stats;
		}

		public function _get__x()
		{
			return $this->x;
		}
		
		public function _get__y()
		{
			return $this->y;
		}
		
		public function _get__three_d()
		{
			return $this->three_d;
		}

		public function _get__image()
		{
			return $this->image;
		}



	// +   M E T H O D S

		private function build()
		{
			// Antialias (if supported)

			if(function_exists(imageantialias))
			{
				imageantialias($this->_get__image(),true);
			}


			// Calculating the central point :

			$posX = round(($this->_get__x()/2));
			$posY = round(($this->_get__y()/2)-($this->_get__three_d()/2));

	
			// Calculating the width and height of pie :

			$w = $this->_get__x()-10;
			$l = $this->_get__y()-10-$this->_get__three_d();

			
			// Allocating colors && Calculating the sum of values :

			$background = imagecolorallocate($this->_get__image(),0,0,0);

			$stats = $this->_get__stats();
			$total = 0;
			$colors = array();
			
			foreach($stats as $valeur)
			{
				$total += $valeur["val"];

				// Let's build an array containing the RVB values in hex. :

				$lclz = array();
				$lclz[] = substr($valeur["color"],1,2);
				$lclz[] = substr($valeur["color"],3,2);
				$lclz[] = substr($valeur["color"],5,2);

				$dclz = array();

				for($i=0 ; $i<3 ; $i++)
				{
				// We get the color values as hexadecimal values (9F for example)

				// Let's convert them into decimal values :

					$lclz[$i] = hexdec($lclz[$i]);
					
				// And then build the darker colors for the 3D effect :

					$dclz[$i] = $lclz[$i] - self::DARKNESS;
					
					if($dclz[$i] < 0)
						$dclz[$i] = 0;
				}

				$light_colors[] = imagecolorallocate($this->_get__image(), $lclz[0], $lclz[1], $lclz[2]);
				$dark_colors[] = imagecolorallocate($this->_get__image(), $dclz[0], $dclz[1], $dclz[2]);
			}

			
			// OK, finally, let's build the pie :

			$nb = count($stats);
			$total_angle=20;


			// First, we add a transparent background :

			imagecolortransparent($this->_get__image(),$background);

	
			// 3D Effect :
			
			if($this->_get__three_d()>0)
			{
				$foo = $posY + $this->_get__three_d();

				for($j=$foo ; $j>$posY ; $j--)
				{
					for($i=0 ; $i<$nb ; $i++)
					{
						$c = $dark_colors[$i];
						$v = $stats[$i]["val"];

						$angle = ($v / $total) * 360;

						imagefilledarc($this->_get__image(),$posX,$j,$w,$l,$total_angle,$angle+$total_angle,$c,IMG_ARC_PIE);
				
						$total_angle = $total_angle + $angle;
					}
				}
			}
	

			// Pie :

			for($i=0 ; $i<$nb ; $i++)
			{
				$c = $light_colors[$i];
				$v = $stats[$i]["val"];
				$n = $stats[$i]["name"];

				$angle = ($v / $total) * 360;

				imagefilledarc($this->_get__image(),$posX,$posY,$w,$l,$total_angle,$angle+$total_angle,$c,IMG_ARC_PIE);
				
				$total_angle = $total_angle + $angle;
			}
		}



		private static function parse_hexcolor($col)
		{
			$r = "0x".substr($col,1,2);
			$v = "0x".substr($col,3,2);
			$b = "0x".substr($col,5,2);

			$res = "$r,$v,$b";

			return $res;
		}



		public function draw()
		{
			header("Content-type: image/png");
			imagepng($this->_get__image());

			imagedestroy($this->_get__image());
		}

		public function save_as($file)
		{
			imagepng($this->_get__image(),$file);

			imagedestroy($this->_get__image());
			
		}
	}
?>