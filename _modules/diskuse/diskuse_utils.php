<?php
	/*
	 *	function dir_size
	 *
	 * 		get the size of the specified directory
	 * 		returns the size of dir in Bytes.
	 *
	 */
	function dir_size($rep)
	{
		$racine = opendir($rep);
		$taille = 0;
		while(false !== ($dossier=readdir($racine)))
		{
			if($dossier!="." && $dossier!="..")
			{
				$p = $rep."/".$dossier;

				if(is_dir($p))
				{
					$taille += dir_size($p);
				}
				else
				{
					$taille += filesize($p);
				}
			}
		}

		closedir($racine);

		return $taille;
	}



 	/*
 	 * 	function get_size_as
 	 *
 	 * 		convert the given size (in Bytes) into the specified unit (KB, MB, GB)
 	 * 		returns the converted size.
 	 *
 	 */
 	function get_size_as($size,$unit="MB")
 	{
 		$res = 0;

 		switch($unit)
 		{
 			case "KB":
 				$res = intval($size/1024)." KiB";
 				break;
 			case "GB":
 				$res = intval($size/(1024*1024*1024))." GiB";
 				break;
 			default:
 				$res = intval($size/(1024*1024))." MiB";
 				break;
		}

		return $res;
	}



	/*
	 * 	function get_size_in_best_unit
	 *
	 * 		convert the given size(in Bytes) into the best unit, depending on the size :
	 * 		ex : 200000B will give 200KB - 2000000B will give 2MB
	 * 		returns the converted size.
	 *
	 */
	function get_size_in_best_unit($size)
	{
		$res = 0;

		if($size >= (1024*1024*1024))
			$res = get_size_as($size,"GB");
		else if($size >= (1024*1024))
			$res = get_size_as($size,"MB");
		else if($size > 1024)
			$res = get_size_as($size,"KB");
		else
			$res = $size;

		return $res;
	}
?>