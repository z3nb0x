<?php
	// REQUIRE

	require_once(dirname(__FILE__)."/../../_inc/template.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");

	require_once(dirname(__FILE__)."/config.inc.php");



	// i18n

	bindtextdomain("dvd",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("dvd","UTF-8");



	// TEMPLATE

	$template = new Template(".");
	$template->set_file("main","../movies/play.tpl");
	$template->set_block("main","ok","letsgo");
	$template->set_block("main","err","error");

	$template->set_var(array(
		"THEME" => $_Z3N__THEME,
		"PLUGIN_TITLE" => dgettext("dvd","DVD"),
		"PLUGIN_IMG" => $_DVD__PLUGIN_IMG,
	));



	// PRETRAITEMENT

	// Ici, vérifier qu'un DVD est bien dans le lecteur et traiter l'erreur le cas échéant.



	// TRAITEMENT

	$template->set_var(array(
		"SRC" => $_DVD__PATH,
		"BACK_LINK" => "../../_menus/menu.php?menu=video",
	));

	$template->parse("letsgo","ok",true);



	// AFFICHAGE

	$template->pparse("out","main");
?>
