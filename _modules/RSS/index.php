﻿<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/FeelRSS.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("RSS",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("RSS","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("RSS", "RSS Feeds"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "../../_menus/menu.php?menu=informations";


	// And finally, the main part

	if(@file_exists($MODULE_CONFIG["FEEDS"]))
	{
		// We prepare the data :

		$_DATA["FEEDS"] = array();

		// Read the given file :
		$feeds = file($MODULE_CONFIG["FEEDS"]);

		if(count($feeds)>1)
		{
		// Allright, there is at least one URL

			// Let's create the RSS feeds :
			foreach($feeds as $url)
			{
				$url = rtrim($url);
				if(!empty($url))
				{
					$new_fil = @FeelRSS::read_RSSFeed($url);

					if(!is_null($new_fil))
					{
						$channel = $new_fil->get__channel();
						$channel->set__channel_link($url);

						$_DATA["FEEDS"][]= array(
							"TITLE" => htmlentities(utf8_decode($channel->get__channel_title())),
							"URL" => urlencode($channel->get__channel_link()),
						);
					}
				}
			}
		}
		else
		{
			// No feed were found
			$_DATA["ERR"] = array(
				"ERROR" => dgettext("rss","No RSS feed found !"),
				"EXPLANATION" => sprintf(dgettext("rss","I wasn't able to find any RSS feed. Please ensure that your <i>%s</i> file is not empty."),$_RSS__FILE),
			);
		}
	}
	else
	{
		// The given file wasn't found
		$_DATA["ERR"] = array(
			"ERROR" => sprintf(dgettext("rss","File <i>%s</i> not found !"),$_RSS__FILE),
			"EXPLANATION" => dgettext("rss","Please ensure that the file exists and that your <i>config.inc.php</i> points to it."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/index.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>