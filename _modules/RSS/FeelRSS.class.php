﻿<?php
/*
 *** --------------------------------
 *     L I C E N C E
 *		Creative-Commons Attribution-ShareAlike 2.0 France
 *** -------------------------------
 *
 *
 *	This file is part of FeelRSS.
 *	Copyright 2006-2007 - François Kubler
 *
 *	This file is published under the Attribution-ShareAlike 2.0 France Creative Commons licence.
 *
 *	This means that you are free :
 *		_ to Share : to copy, distribute, display, and perform the work
 *		_ to Remix : to make derivative works
 *
 *	But you also HAVE TO :
 *		_ attribute the work in the manner specified by the author or licensor.
 *		_ share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.
 *
 *	Basically, you can do whatever you want with this file, including commercial use of it but you have to specify my name and redistribute your work under the same license.
 *	More details can be found here : http://creativecommons.org/licenses/by-sa/2.0/fr/
 *
 *
 *** --------------------------------
 *     I N F O R M A T I O N
 *** --------------------------------
 *
 *	FeelRSS is a small group of PHP5 classes written to manipulate RSS 2.0 data feeds with ease.
 *	Those classes are brought to you without any warranty !
 *
 *	FeelRSS consists of : 
 *		¤ FeelRSS class
 *		¤ ChannelRSS class
 *		¤ ItemRSS class
 *
 *	You can use them to read and/or write RSS 2.0 feeds.
 *
 *	For further information, please read 
 *
 *** --------------------------------
 */


	
	class FeelRSS
	{
		// --- A T T R I B U T S ---------------------------------------------------------------------------


		const CLASS_VERSION = "20070209";
		const RSS_VERSION = "2.0";

		private $channel;				// Channel of the feed
		private $itemslist;				// List of items





		// --- C O N S T R U C T E U R ---------------------------------------------------------------------


		public function __construct($title,$link,$description,$copyright="",$date="")
		{
			$this->set__channel(new ChannelRSS($title,$link,$description,$copyright,$date));
			$this->set__itemslist(array());
		}





		// --- M E T H O D E S -----------------------------------------------------------------------------


		// ACCESSEURS ____________________________


		public function get__class_version()
		{
			return self::CLASS_VERSION;
		}

		public function get__rss_version()
		{
			return self::RSS_VERSION;
		}

		public function get__channel()
		{
			return $this->channel;
		}

		public function get__itemslist()
		{
			return $this->itemslist;
		}


		public function set__channel($newchannel)
		{
			$this->channel = $newchannel;
		}

		public function set__itemslist($items)
		{
			$this->itemslist = $items;
		}



		// METHODES _______________________________
	

		public function get_item($pos)
		{
			$l = $this->get__itemslist();
			
			return $l[$pos];
		}



		public function add_item($item)
		{
			$l = $this->get__itemslist();
			$l[] = $item;

			$this->set__itemslist($l);
		}
	
	

		public function nbitems()
		{
			return count($this->get__itemslist());
		}


		public function make_RSSFeed()
		{
			$channel = $this->get__channel();
			$items = $this->get__itemslist();

			$xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
			$xml .= "<rss version=\"".$this->get__rss_version()."\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\">\n";
			
			$xml .= $channel->write_channel();			
		
			foreach($items as $item)
			{
				$xml .= $item->write_item();
			}

			$xml .= "\t</channel>\n";
			$xml .= "</rss>";

			return $xml;
		}


		public function write_RSSFeed($fichier)
		{
			$ret = false;
			$xml = $this->make_RSSFeed();
		
			$fd = @fopen($fichier,"w+");
				
			if(!$fd)
			{
				trigger_error("<br />_ FeelRSS error : [".__METHOD__."] Impossible d'ouvrir le fichier en écriture !");
			}
			else
			{
				@fputs($fd,$xml);
				@fclose($fd);
				echo("<br /> _ FeelRSS : Le fil RSS <i><b>".$channel->get__channel_title()."</b></i> a été écrit correctement.");
				$ret = true;
			}
			
			return $ret;
		}

		
		public function get_RSSFeed()
		{
			header("content-type: application/xml");
			echo $this->make_RSSFeed();
		}



		// Statiques ----------------------------



		public static function read_RSSFeed($fichier)
		{
			$ret = NULL;

			$xml = new DOMDocument();
			$ok = @$xml->load($fichier);

			if(!$ok)
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Le fichier spécifié n'existe pas !");
			}
			else
			{
				// Parsing (DOM) et initialisation du FeelRSS
				
				$items = $xml->getElementsByTagName("channel");
				
				$chan = $items->item(0);

				$title = "";
				$desription = "";
				$link = "";
				$pubDate = "";
				$copyright = "";
				$items = array();

				foreach($chan->childNodes as $node)
				{
					switch($node->nodeName)
					{
						case "title":
							$title = $node->nodeValue;
							break;

						case "description":
							$description = $node->nodeValue;
							break;

						case "link":
							$link = $node->nodeValue;
							break;

						case "pubDate":
							$pubDate = $node->nodeValue;
							break;

						case "copyright":
							$copyright = $node->nodeValue;
							break;

						case "item":
							$i_title = "";
							$i_description = "";
							$i_link = "";
							$i_pubDate = "";
								
							foreach($node->childNodes as $item_prop)
							{
								switch($item_prop->nodeName)
								{
									case "title":
										$i_title = $item_prop->nodeValue;
										break;

									case "description":
										$i_description = $item_prop->nodeValue;
										break;

									case "link":
										$i_link = $item_prop->nodeValue;
										break;

									case "pubDate":
										$i_pubDate = $item_prop->nodeValue;
										break;
								}
							}

							$items[] = new ItemRSS($i_title,$i_link,$i_description,$i_pubDate);
							break;
					}
				}
				
				$ret = new FeelRSS($title,$link,$description,$copyright,$pubDate);
				$ret->set__itemslist($items);
			}

			return $ret;
		}
	}




// ===================================================================================================================




	class ChannelRSS
	{
		// --- A T T R I B U T S ---------------------------------------------------------------------------


		const CHANNEL_GENERATOR = "FeelRSS";

		private $channel_title;
		private $channel_link;
		private $channel_description;
		private $channel_copyright;
		private $channel_date_publication;





		// --- C O N S T R U C T E U R ---------------------------------------------------------------------


		public function __construct($title,$link,$description,$copyright="",$date="")
		{
			$this->set__channel_title($title);
			$this->set__channel_link($link);
			$this->set__channel_description($description);
			$this->set__channel_copyright($copyright);
			$this->set__channel_date_publication($date);
		}





		// --- M E T H O D E S -----------------------------------------------------------------------------


		// ACCESSEURS ____________________________

		public function get__channel_title()
		{
			return $this->channel_title;
		}

		public function get__channel_link()
		{
			return $this->channel_link;
		}
		
		public function get__channel_description()
		{
			return $this->channel_description;
		}

		public function get__channel_date_publication()
		{
			return $this->channel_date_publication;
		}

		public function get__channel_generator()
		{
			return self::CHANNEL_GENERATOR;
		}

		public function get__channel_copyright()
		{
			return $this->channel_copyright;
		}


		public function set__channel_title($title)
		{
			if(isset($title) && !empty($title))
			{
				$this->channel_title = $title;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier un <u>title</u>");
			}
		}

		public function set__channel_link($link)
		{
			if(isset($link) && !empty($link))
			{
				$this->channel_link = $link;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier un <u>link</u>");
			}
		}
		
		public function set__channel_description($description)
		{
			if(isset($description) && !empty($description))
			{
				$this->channel_description = $description;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier une <u>description</u>");
			}
		}

		public function set__channel_copyright($copyright)
		{
			if(isset($copyright) && !empty($copyright))
			{
				$this->channel_copyright = $copyright;
			}
		}

		public function set__channel_date_publication($pub)
		{
			if(!empty($pub))
				$this->channel_date_publication = $pub;
			else
				$this->channel_date_publication = date("r",time());
		}



		// AUTRES METHODES _______________________

		public function write_channel()
		{
			$xml = "\t<channel>\n";
			$xml .= "\t\t<title>".$channel->get__channel_title()."</title>\n";
			$xml .= "\t\t<link>".$channel->get__channel_link()."</link>\n";
			$xml .= "\t\t<description>".$channel->get__channel_description()."</description>\n";
			$xml .= "\t\t<copyright>".$channel->get__channel_copyright()."</copyright>\n";
			$xml .= "\t\t<generator>".$channel->get__channel_generator()." - ".$this->get__class_version()."</generator>\n";
			$xml .= "\t\t<pubDate>".$channel->get__channel_date_publication()."</pubDate>\n";
			$xml .= "\t\t<lastBuildDate>".$channel->get__channel_date_publication()."</lastBuildDate>\n";

			return $xml;
		}
	}



// ===================================================================================================================



	class ItemRSS
	{
		// --- A T T R I B U T S ---------------------------------------------------------------------------

	
		private $item_title;
		private $item_link;
		private $item_description;
		private $item_pubDate;

		



		// --- C O N S T R U C T E U R ---------------------------------------------------------------------


		public function __construct($title,$link,$description="",$date="")
		{
			$this->set__item_title($title);
			$this->set__item_link($link);
			$this->set__item_description($description);
			$this->set__item_pubDate($date);
		}
		




		// --- M E T H O D E S -----------------------------------------------------------------------------


		// ACCESSEURS ____________________________

		public function get__item_title()
		{
			return $this->item_title;
		}

		public function get__item_link()
		{
			return $this->item_link;
		}

		public function get__item_description()
		{
			return $this->item_description;
		}

		public function get__item_pubDate()
		{
			return $this->item_pubDate;
		}


		public function set__item_title($title)
		{
			if(isset($title) && !empty($title))
			{
				$this->item_title = $title;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier un <u>title</u>");
			}
		}

		public function set__item_link($link)
		{
			if(isset($link))
			{
				$this->item_link = $link;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier un <u>link</u>");
			}
		}

		public function set__item_description($description)
		{
			if(isset($description))
			{
				$this->item_description = $description;
			}
			else
			{
				trigger_error("<br /> _ FeelRSS error : [".__METHOD__."] Vous devez spécifier une <u>description</u>");
			}
		}

		public function set__item_pubDate($pub)
		{
			if(isset($pub) && !empty($pub))
			{ 
				$this->item_pubDate = $pub;
			}
			else
			{
				$this->item_pubDate = date("r",time());
			}
		}



		// AUTRES METHODES _______________________

		public function write_item()
		{
			$xml = "\t\t<item>\n";
			$xml .= "\t\t\t<title>".$item->get__item_title()."</title>\n";					
			$xml .= "\t\t\t<link>".$item->get__item_link()."</link>\n";
			$xml .= "\t\t\t<pubDate>".$item->get__item_pubDate()."</pubDate>\n";
			$xml .= "\t\t\t<category></category>\n";
			$xml .= "\t\t\t<guid>".$item->get__item_link()."</guid>\n";
			$xml .= "\t\t\t<description>".$item->get__item_description()."</description>\n";
			$xml .= "\t\t</item>\n";

			return $xml;
		}
	}
?>