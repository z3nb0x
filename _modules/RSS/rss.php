﻿<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/FeelRSS.class.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("RSS",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("RSS","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("RSS", "RSS Feeds"),
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "index.php";


	// And finally, the main part

	$url = urldecode($_GET["url"]);

	$fil = @FeelRSS::read_RSSFeed($url);

	if(!is_null($fil))
	{
		$chan = $fil->get__channel();
		$items = $fil->get__itemslist();

		$_DATA["FEED_TITLE"] = htmlentities(utf8_decode($chan->get__channel_title()));
		$_DATA["ENTRY"] = array();

		foreach($items as $item)
		{
			$_DATA["ENTRY"][] = array(
				"URL" => urlencode($item->get__item_link()),
				"TITLE" => htmlentities(utf8_decode($item->get__item_title())),
				"DESCRIPTION" => htmlentities(utf8_decode($item->get__item_description())),
			);
		}
	}
	else
	{
		$_DATA["ERR"] =  array(
			"ERROR" => dgettext("rss",""),
			"EXPLANATION" => dgettext("rss",""),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/rss.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>