<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/player.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
	<script type="text/javascript" src="../../_js/ajax.js"></script>
	<script type="text/javascript" src="_js/audio.js"></script>
	<script type="text/javascript" src="_js/fullscreen.js"></script>
</head>
<body>
	<div id="playing_info">
		<img src="<?php echo $_DATA["COVER"]; ?>" alt="<?php echo $_DATA["ARTIST"]." - ".$_DATA["ALBUM"]; ?>" id="cover" />

		<div id="nfo">
			<p id="artist"><?php echo $_DATA["ARTIST"]; ?></p>
			<p id="album"><?php echo $_DATA["ALBUM"]; ?></p>
			<p id="title"><?php echo $_DATA["SONG"]; ?></p>
		</div>
	</div>
	<input type="hidden" value="<?php echo $_DATA["THEME"]; ?>" name="theme" id="theme" />

	<ul class="controls">
		<li>
			<a href="" id="btn_prev"><img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/back.png" /></a>
		</li>
		<li>
			<a href=""id="btn_play"><img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/play.png" id="img_play" /></a>
		</li>
		<li>
			<a href="" id="btn_stop"><img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/stop.png" /></a>
		</li>
		<li>
			<a href="" id="btn_next" class="space"><img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/next.png" /></a>
		</li>
		<li>
			<a href="index.php" id="go_to_library" class="space"><img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/library.png" /></a>
		</li>
	</ul>
</body>
</html>