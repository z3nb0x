<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<title>z3nb0x ___ [module:<?php echo $_DATA["MODULE"]; ?>]</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" href="../../favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../_themes/<?php echo $_DATA["THEME"]; ?>/css/player.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
	<script type="text/javascript" src="../../_js/ajax.js"></script>
	<script type="text/javascript" src="_js/audio.js"></script>
	<script type="text/javascript" src="_js/imageflow.js"></script>
</head>
<body>
	<h1><?php echo $_DATA["TITLE"]; ?></h1>
	<img class="plugin_img" alt="" src="<?php echo $_DATA["IMG"]; ?>" />

	<div id="overlay">
		<div id="loading" class="loading">
			<p><?php echo $_DATA["LOADING"]; ?></p>
		</div>
	</div>

	<div id="imageflow">
		<div id="images">
			<?php foreach($_DATA["ALBUMS"] as $a): ?>
			<img src="create_cover.php?cover=<?php echo $a["COVER"]; ?>" alt="<?php echo $a["ARTIST"]." - ".$a["ALBUM"]; ?>" rel="<?php echo $a["ALBUM"]; ?>" />
			<?php endforeach; ?>
		</div>
		<div id="captions"></div>
		<div id="scrollbar">
			<div id="slider"></div>
		</div>
	</div>
	<ul class="controls">
		<li>
			<a href="fullscreen.php" title="<?php echo $_DATA["TXT_FULLSCREEN"]; ?>" class="space">
				<img src="../../_themes/<?php echo $_DATA["THEME"]; ?>/images/player/playing.png" alt="<?php echo $_DATA["TXT_FULLSCREEN"]; ?>" />
			</a>
		</li>
	</ul>

	<a id="back" href="<?php echo $_DATA["BACK_LINK"]; ?>">&lt;</a>
</body>
</html>