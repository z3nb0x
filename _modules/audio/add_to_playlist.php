<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/MPD_Controller.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// _____________________________________________________________________

	$player = @new MPD_Controller($MODULE_CONFIG["HOST"],$MODULE_CONFIG["PORT"],$MODULE_CONFIG["PASS"]);

	if($player->is_connected())
	{
		$artist = rawurldecode($_GET["art"]);
		$album = rawurldecode($_GET["alb"]);
		$song = rawurldecode($_GET["son"]);

		$s = array();

		if(!empty($song))
		{
			$s[]["file"] = $song;
		}
		else
		{
			if(!empty($album) && $album != "all")
			{
				$s = $player->list_songs_of_album($album);
			}
			else
			{
				if($artist != "all")
					$s = $player->list_songs_of_artist($artist);
				else
					$s = $player->list_songs();
			}
		}

		foreach($s as $chanson)
		{
			$player->playlist_add($chanson["file"]);
		}
	}
	else
	{
		// ...
	}

	$player->disconnect();
?>