<?php
	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/Image.class.php");


	if(isset($_GET["cover"]) && !empty($_GET["cover"]))
	{
		$cover = urldecode($_GET["cover"]);
		$cover_sum = "refl_".md5($cover).".png";
		$cover_path = $Z3NB0X_CONFIG["CACHE_PATH"]."/".$cover_sum;

		if(file_exists($cover_path) && filemtime($cover_path) >= filemtime($cover))
		{
			// We use the cached image

			header("Content-type: image/png");
			readfile($cover_path);
			exit();
		}
		else
		{
			// We build a new image

			$pic = new Image($cover);
			$pic->resize(350,350);
			$pic->create_reflection(30,40);

			$pic->save("PNG",$cover_path);
			//chown($cover_path,"z3nb0x");
			chgrp($cover_path,"z3nb0x");
			chmod($cover_path,0755);
			$pic->draw("PNG");
		}
	}
	else
	{
		exit();
	}
?>