<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/MPD_Controller.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("audio",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("audio","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"IMG" => $MODULE_CONFIG["IMG"],
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We don't need a backlink

	// And finally, the main part

	$player = @new MPD_Controller($MODULE_CONFIG["HOST"],$MODULE_CONFIG["PORT"],$MODULE_CONFIG["PASS"]);

	if($player->is_connected())
	{
		$artist = "";
		$title = "";
		$album = "";
		$cover = $MODULE_CONFIG["DEFAULT_COVER"];

		if($player->a_song_is_playing())
		{
			$song_info = $player->get_current_song_info();

			$artist = $song_info["Artist"];
			$album = $song_info["Album"];
			$title = $song_info["Title"];

			$possible_cover = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"]."/".$artist."/".$album."/cover.jpg";

			if(file_exists($possible_cover))
				$cover = $possible_cover;
		}

		$cover = "create_cover.php?cover=".urlencode($cover);

		$_DATA["COVER"] = $cover;
		$_DATA["ARTIST"] = htmlentities(utf8_decode($artist));
		$_DATA["ALBUM"] = htmlentities(utf8_decode($album));
		$_DATA["SONG"] = htmlentities(utf8_decode($title));

		$player->disconnect();
	}
	else
	{
		$_DATA["ERR"] = array(
			"ERROR" => sprintf(dgettext("audio", "No connexion on %s:%s"), $MODULE_CONFIG["HOST"], $MODULE_CONFIG["PORT"]),
			"EXPLANATION" => dgettext("audio", "I wasn't able to connect to MPD. Please check your <i>config.inc.php</i> file and ensure that MPD is running :)"),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/fullscreen.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>