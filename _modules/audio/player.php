<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/MPD_Controller.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// _____________________________________________________________________

	$player = @new MPD_Controller($MODULE_CONFIG["HOST"],$MODULE_CONFIG["PORT"],$MODULE_CONFIG["PASS"]);

	if($player->is_connected())
	{
		switch($_GET["cmd"])
		{
			case "play":
				$player->play();
				break;
			case "stop":
				$player->stop();
				break;
			case "next":
				$player->next();
				break;
			case "prev":
				$player->previous();
				break;
			case "clear":
				$player->playlist_clear();
				break;
			case "del_song":
				$player->playlist_delete($_GET["id"]);
				break;
		}
	}
	else
	{
		// ...
	}

	$player->disconnect();
?>