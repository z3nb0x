<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/MPD_Controller.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// _____________________________________________________________________

	$res = "<?xml version=\"1.0\"?>\n";

	$player = @new MPD_Controller($MODULE_CONFIG["HOST"],$MODULE_CONFIG["PORT"],$MODULE_CONFIG["PASS"]);

	if($player->is_connected())
	{
		$repeat = 0;
		if($player->_get__repeat())
			$repeat = 1;

		$random = 0;
		if($player->_get__random())
			$random = 1;

		$artist = "";
		$title = "";
		$album = "";
		$pos = -1;
		$songid = -1;
		$cover = $MODULE_CONFIG["DEFAULT_COVER"];

		if($player->a_song_is_playing())
		{
			$song_info = $player->get_current_song_info();

			$artist = $song_info["Artist"];
			$album = $song_info["Album"];
			$title = $song_info["Title"];
			$pos = $song_info["Pos"];

			$possible_cover = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"]."/".$artist."/".$album."/cover.jpg";

			if(file_exists($possible_cover))
				$cover = $possible_cover;
		}

		$res = "<?xml version=\"1.0\"?>\n";
		$res .= "<status>\n";
		$res .= "\t<state>".$player->_get__state()."</state>\n";
		$res .= "\t<volume>".$player->_get__volume()."</volume>\n";
		$res .= "\t<artist>".$artist."</artist>\n";
		$res .= "\t<album>".$album."</album>\n";
		$res .= "\t<title>".$title."</title>\n";
		$res .= "\t<pos>".$pos."</pos>\n";
		$res .= "\t<cover>".$cover."</cover>\n";
		$res .= "</status>";

		$player->disconnect();
	}
	else
	{
		$res = "<err>Not connexion !</err>\n";
	}





	// SEND THE ANSWER _____________________________________________________

	header("Content-Type: text/xml");
	echo $res;
?>