<?php
/*
 * ----------------------------
 *
 * Audio Plugin [Part of z3nb0x]
 *
 * - - - - - -
 *
 *	Class PHP permettant de controler MPD (http://www.musicpd.org/)
 *
 *	Thanks to http://mpd.wikia.com/wiki/MusicPlayerDaemonCommands
 *
 * ----------------------------
 */
/*
	TODO :
		$this->get_uptime_as_string()						-> tester
		Tester les méthodes écrites dernièrement (voir functions.txt)		-> todo
 */

	class MPD_Controller
	{
		const CLASS_VERSION = "2007-0427";				// Version of PHP class

	// +   A T T R I B U T E S

		private $lang;							// Lang used

		private $server;						// Address of server that runs MPD
		private $port;							// Port on which MPD runs

		private $socket;						// Socket used to communicate
		private $connected;						// True if we are connected to MPD, false otherwise

		private $state;							// State of the player
		private $volume;						// Volume of the player
		private $random;						// true if the player plays the songs randomly
		private $repeat;						// true if the player plays thes songs continuously
		private $crossfade;						// crossfade of player

		private $playlist;						// The current playlist
		private $current_song_id;					// ID of the song playing or paused

		private $nb_artists;						// Number of artists in database
		private $nb_albums;						// Number of albums in database
		private $nb_songs;						// Number of songs in database
		private $uptime;						// Daemon uptime (time since last startup) in seconds
		private $playtime;						// Time length of music played
		private $db_playtime;						// Sum of all song times in db
		private $db_update;						// Last db update in UNIX time



		private $authorized_commands = array(				// Authorized commands
			"password",
			"status",
			"stats",
			"setvol",
			"playlistinfo",
			"add",
			"delete",
			"clear",
			"play",
			"pause",
			"stop",
			"currentsong",
			"next",
			"previous",
			"update",
			"list",
			"listallinfo",
			"crossfade",
		);

		private $states = array(					// Translations for the state of the player
						"en" => array(
							"play" => "Playing",
							"pause" => "Paused",
							"stop" => "Stopped",
						),

						"fr" => array(
							"play" => "En cours de lecture",
							"pause" => "En pause",
							"stop" => "Stop",
						),
		);



	// +   C O N S T R U C T O R

		public function __construct($_server, $_port, $_pass="", $lang="en")
		{
			$this->_set__server($_server);
			$this->_set__port($_port);

			if($this->connect()===false)
				return NULL;

			if(!empty($_pass))
			{
				if($this->password($_pass)===false)
				{
					return NULL;
				}
			}

			$this->_set__lang($lang);

			if($this->status()===false)
				return NULL;

			if($this->stats()===false)
				return NULL;

			$this->playlist();
		}



	// +   A C C E S S O R S

		private function _set__server($_server)
		{
			$this->server = $_server;
		}

		private function _set__port($_port)
		{

			$this->port = $_port;
		}

		private function _set__lang($_lang)
		{
			if(array_key_exists($_lang,$this->_get__states()))
				$this->lang = $_lang;
			else
			{
				$kstates = array_keys($this->_get__states());
				$this->lang = $kstates[0];
			}
		}

		private function _set__socket($sock)
		{
			$this->socket = $sock;
		}

		private function _set__connected($con)
		{
			$this->connected = $con;
		}

		private function _set__state($state)
		{
			$this->state = $state;
		}

		private function _set__volume($vol)
		{
			$this->volume = $vol;
		}

		private function _set__random($random)
		{
			if($random=="1")
				$this->random = true;
			else
				$this->random = false;
		}

		private function _set__repeat($repeat)
		{
			if($repeat=="1")
				$this->repeat = true;
			else
				$this->repeat = false;
		}

		private function _set__crossfade($xfade)
		{
			$this->crossfade = $xfade;
		}

		private function _set__nb_artists($artists)
		{
			$this->nb_artists = $artists;
		}

		private function _set__nb_albums($albums)
		{
			$this->nb_albums = $albums;
		}

		private function _set__nb_songs($songs)
		{
			$this->nb_songs = $songs;
		}

		private function _set__uptime($uptime)
		{
			$this->uptime = $uptime;
		}

		private function _set__playtime($playtime)
		{
			$this->playtime = $playtime;
		}

		private function _set__db_playtime($db_playtime)
		{
			$this->db_playtime = $db_playtime;
		}

		private function _set__db_update($db_update)
		{
			$this->db_update = $db_update;
		}

		private function _set__playlist($play)
		{
			$this->playlist = $play;
		}

		private function _set__current_song_id($id)
		{
			$this->current_song_id = $id;
		}



	// ------------------------------------------------


		public function _get__server()
		{
			return $this->server;
		}

		public function _get__port()
		{
			return $this->port;
		}

		public function _get__lang()
		{
			return $this->lang;
		}

		public function _get__socket()
		{
			return $this->socket;
		}

		public function _get__connected()
		{
			return $this->connected;
		}

		public function _get__state()
		{
			return $this->state;
		}

		public function _get__volume()
		{
			return $this->volume;
		}

		public function _get__random()
		{
			return $this->random;
		}

		public function _get__repeat()
		{
			return $this->repeat;
		}

		public function _get__crossfade()
		{
			return $this->crossfade;
		}

		public function _get__nb_artists()
		{
			return $this->nb_artists;
		}

		public function _get__nb_albums()
		{
			return $this->nb_albums;
		}

		public function _get__nb_songs()
		{
			return $this->nb_songs;
		}

		public function _get__uptime()
		{
			return $this->uptime;
		}

		public function _get__playtime()
		{
			return $this->playtime;
		}

		public function _get__db_playtime()
		{
			return $this->db_playtime;
		}

		public function _get__db_update()
		{
			return $this->db_update;
		}

		public function _get__playlist()
		{
			return $this->playlist;
		}

		public function _get__current_song_id()
		{
			return $this->current_song_id;
		}


	// ------------------------------------------------


		private function _get__authorized_commands()
		{
			return $this->authorized_commands;
		}

		private function _get__states()
		{
			return $this->states;
		}

		static function _get__class_version()
		{
			return self::CLASS_VERSION;
		}



	// +   M E T H O D S

		private function connect()
		{
			$ret = false;

			$this->_set__socket(@fsockopen($this->_get__server(), $this->_get__port(),$errno,$errstr,10));

			if(!$this->_get__socket())
			{
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] ".$errno." : ".$errstr." !",E_USER_WARNING);
			}
			else
			{

				while(!feof($this->_get__socket()))
				{
					$response = fgets($this->_get__socket(),1024);

					if(strncmp("OK",$response,2)==0)
					{
						$ret = true;
						$this->_set__connected(true);
						break;
					}
				}
			}

			return $ret;
		}


		public function send_command($command,$arg1="",$arg2="",$arg3="")
		{
			$res = "";

			if(!in_array($command,$this->_get__authorized_commands()))
			{
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {".$command."} Unknown/unauthorized command detected !",E_USER_WARNING);
				$res = false;
			}
			else
			{
				$cmd = $command;

				if(!empty($arg1))
				{
					$cmd .= " \"$arg1\"";
				}

				if(!empty($arg2))
				{
					$cmd .= " \"$arg2\"";
				}

				if(!empty($arg3))
				{
					$cmd .= " \"$arg3\"";
				}

				fputs($this->_get__socket(),"$cmd\r\n");

				while(!feof($this->_get__socket()))
				{
					$response = fgets($this->_get__socket(),1024);

					if(strncmp("OK",$response,2)==0)
					{
						break;
					}

					if(strncmp("ACK",$response,3)==0)
					{
						$res = false;
						trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {".$command."} send_command failed !",E_USER_WARNING);
						break;
					}

					$res .= $response;
				}
			}

			return $res;
		}


		private function password($pass)
		{
			$res = $this->send_command("password",$pass);

			if($res===false)
				$this->disconnect();

			return $res;
		}


		private function status()
		{
			$res = false;

			$val = $this->send_command("status");

			if($val===false)
			{
				$this->disconnect();
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {status} enable to get the status of player !",E_USER_WARNING);
			}
			else
			{
				$p = array();
				$lines = explode("\n",rtrim($val));

				foreach($lines as $line)
				{
					list($param,$value) = split(": ",$line);
					$p[$param] = $value;
				}

				$this->_set__state($p["state"]);
				$this->_set__volume($p["volume"]);
				$this->_set__random($p["random"]);
				$this->_set__repeat($p["repeat"]);
				$this->_set__crossfade($p["xfade"]);
				$this->_set__current_song_id(-1);

				if($p["state"]=="play" || $p["state"]=="pause")
					$this->_set__current_song_id($p["songid"]);

				$res = true;
			}

			return $res;
		}


		public function stats()
		{
			$res = false;

			$val = $this->send_command("stats");

			if($val===false)
			{
				$this->disconnect();
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {stats} enable to get the statistics !",E_USER_WARNING);
			}
			else
			{
				$p = array();
				$lines = explode("\n",rtrim($val));

				foreach($lines as $line)
				{
					list($param,$value) = split(": ",$line);
					$p[$param] = $value;
				}

				$this->_set__nb_artists($p["artists"]);
				$this->_set__nb_albums($p["albums"]);
				$this->_set__nb_songs($p["songs"]);
				$this->_set__uptime($p["uptime"]);
				$this->_set__playtime($p["playtime"]);
				$this->_set__db_playtime($p["db_playtime"]);
				$this->_set__db_update($p["db_update"]);

				$res = true;
			}

			return $res;
		}


		private function playlist()
		{
			$this->_set__playlist(array());

			$val = $this->get_playlistinfo();

			if($val!==false)
				$this->_set__playlist($this->create_playlist($val));
		}

		private function get_playlistinfo()
		{
			$res = false;

			$val = $this->send_command("playlistinfo");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {get_playlistinfo} enable to get information !",E_USER_WARNING);
			else
				$res = $val;

			return $res;
		}

		private function create_playlist($val)
		{
			$res = array();

			if(!empty($val))
			{
				$lines = explode("\n",rtrim($val));

				$n = -1;
				foreach($lines as $line)
				{
					list($param,$value) = split(": ",$line);

					if($param=="file")
					{
						$n++;
					}

					$res[$n][$param] = $value;
				}
			}

			return $res;
		}

		public function db_list($meta1,$meta2="",$search="")
		{
			$res = false;

			if(!empty($meta2) && !empty($search))
				$val = @$this->send_command("list",$meta1,$meta2,$search);
			else
				$val = @$this->send_command("list",$meta1);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {db_list} enable to get the list of <i>".$meta1."</i> !",E_USER_WARNING);
			else
			{
				$res = array();
				$lines = explode("\n",rtrim($val));

				foreach($lines as $line)
				{
					list($param,$value) = split(": ",$line);

					if($param==$meta1)
						$res[] = $value;
				}
			}

			sort($res);

			if($meta1 == "file")
			{
				$r = array();

				foreach($res as $f)
				{
					$bn = basename($f);
					$t = substr($bn,0,strlen($bn)-4);

					$r[] = array("file" => $f, "title" => $t);
				}

				$res = $r;
			}

			return $res;
		}


	// ------------------------------------------------


		public function disconnect()
		{
			@fclose($this->_get__socket());
			$this->_set__socket(NULL);
			$this->_set__connected(false);
		}


		public function is_connected()
		{
			return $this->_get__connected();
		}


		public function get_state_as_string()
		{
			$states = $this->_get__states();
			return $states[$this->_get__lang()][$this->_get__state()];
		}


		public function get_uptime_as_string()
		{
			$sec = $this->_get__uptime();

			$timestamp = mktime(0,0,$sec,0,0,0);

			$h = floor($timestamp/3600);
			$m = floor(($timestamp - ($h*3600))/60);

			return $h."H ".$m;
		}


		public function a_song_is_playing()
		{
			$res = false;

			if($this->_get__current_song_id()!=-1)
				$res = true;

			return $res;
		}


		public function get_current_song_info()
		{
			$res= array();

			if($this->a_song_is_playing())
			{
				$val = $this->send_command("currentsong");

				if($val===false)
					trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {get_current_song_info} enable to get the information !",E_USER_WARNING);
				else
				{
					$lines = explode("\n",rtrim($val));

					foreach($lines as $line)
					{
						list($param,$value) = split(": ",$line);
						$res[$param] = $value;
					}
				}
			}
			else
			{
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {get_current_song_info} no song playing !",E_USER_WARNING);
			}

			return $res;
		}


		public function volume($vol)
		{
			$res = false;

			if(is_numeric($vol))
			{
				if($vol<0)
					$vol = 0;
				if($vol>100)
					$vol = 100;

				if($res = $this->send_command("setvol",$vol)!==false);
					$this->_set__volume($vol);
			}
			else
			{
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {volume} not a numeric value !",E_USER_WARNING);
			}

			return $res;
		}


		public function playslists()
		{
			$res= array();

			$val = $this->send_command("lsinfo");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlists} enable to get the available playlist(s) !",E_USER_WARNING);
			else
			{
				$lines = explode("\n",rtrim($val));

				foreach($lines as $line)
				{
					list($param,$value) = split(": ",$line);

					if($param=="playlist")
						$res[] = $value;
				}
			}

			return $res;
		}

		public function playlist_load($pl)
		{
			$res = false;

			$val = $this->send_command("load",$pl);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_load} enable to load the playlist <i>".$pl."</i> !",E_USER_WARNING);
			else
			{
				$this->playlist();
				$res = true;
			}

			return $res;
		}


		public function playlist_add($song)
		{
			$res = false;

			$val = $this->send_command("add",$song);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_add} enable to add <i>".$song."</i> to the playlist !",E_USER_WARNING);
			else
			{
				$this->playlist();
				$res = true;
			}

			return $res;
		}

		public function playlist_delete($song)
		{
			$res = false;

			$val = $this->send_command("delete",$song);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_delete} enable to delete <i>".$song."</i> of the playlist !",E_USER_WARNING);
			else
			{
				$this->playlist();
				$res = true;
			}

			return $res;
		}

		public function playlist_clear()
		{
			$res = false;

			$val = $this->send_command("clear");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_clear} enable to clear the playlist !",E_USER_WARNING);
			else
			{
				$this->playlist();
				$res = true;
			}

			return $res;
		}

		public function playlist_repeat()
		{
			$res =false;

			if($this->_get__repeat())
				$val = $this->send_command("repeat",0);
			else
				$val = $this->send_command("repeat",1);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_repeat} enable to make the playlist repeat !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}

		public function playlist_random()
		{
			$res =false;

			if($this->_get__random())
				$val = $this->send_command("random",0);
			else
				$val = $this->send_command("random",1);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {playlist_random} enable to make the playlist random !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}


		public function crossfade($nbsec)
		{
			if($nbsec<0)
				$nbsec = 0;
			if($nbsec>10)
				$nbsec = 10;

			$val = $this->send_command("crossfade",$nbsec);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {crossfade} enable to set crossfade !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}
		}


		public function play()
		{
			$res = false;

			$state = $this->_get__state();

			if($state=="pause")
				$val = $this->send_command("pause",0);
			else if($state=="stop")
				$val = $this->send_command("play");
			else
				$val = $this->send_command("pause",1);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {play} enable to play !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}

		public function pause()
		{
			$res = false;

			$paused = $this->_get__state();

			if($paused=="pause")
				$val = $this->send_command("pause",0);
			else
				$val = $this->send_command("pause",1);

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {pause} enable to pause !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}

		public function stop()
		{
			$res = false;

			$val = $this->send_command("stop");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {stop} enable to stop !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}

		public function next()
		{
			$res = false;

			$val = $this->send_command("next");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {next} enable to jump to the next song !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}

		public function previous()
		{
			$res = false;

			$val = $this->send_command("previous");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {previous} enable to jump to the previous song !",E_USER_WARNING);
			else
			{
				if($this->status()!==false)
					$res = true;
			}

			return $res;
		}


		public function db_update()
		{
			$res = false;

			$val = $this->send_command("update");

			if($val===false)
				trigger_error("<br />_ MPD_Controller error : [MPD_Controller] {db_update} enable to update the database !",E_USER_WARNING);
			else
			{
				if($this->stats()!==false)
					$res = true;
			}

			return $res;
		}


		public function list_artists()
		{
			return $this->db_list("Artist");
		}

		public function list_albums()
		{
			$res = array();

			$artists = $this->db_list("Artist");

			foreach($artists as $a)
			{
				$albums = $this->list_albums_of_artist($a);

				foreach($albums as $l)
				{
					$res[] = array(
							"artist" => $a,
							"album" => $l["album"],
					);
				}
			}
			return $res;
		}

		public function list_albums_of_artist($artist)
		{
			$res = array();
			$albums = $this->db_list("Album","Artist",$artist);

			foreach($albums as $a)
			{
				$res[] = array(
						"artist" => $artist,
						"album" => $a
				);
			}

			return $res;
		}

		public function list_songs_of_artist($artist)
		{
			return $this->db_list("file","Artist",$artist);
		}

		public function list_songs_of_album($album)
		{
			return $this->db_list("file","Album",$album);
		}

		public function list_songs()
		{
			return $this->db_list("file");
		}
	}
?>
