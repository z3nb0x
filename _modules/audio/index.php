<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/../../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../../_inc/i18n.inc.php");
	require_once(dirname(__FILE__)."/MPD_Controller.php");

	$MODULE_CONFIG = parse_ini_file(dirname(__FILE__)."/MODULE.ini");





	// i18n ________________________________________________________________

	bindtextdomain("audio",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("audio","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$_DATA = array(
		"MODULE" => htmlentities(utf8_decode(basename(dirname(__FILE__)))),
		"THEME" => $Z3NB0X_CONFIG["THEME"],
		"TITLE" => dgettext("audio", "Music Library"),
		"IMG" => $MODULE_CONFIG["IMG"],
		"LOADING" => gettext("Loading..."),
		"TXT_FULLSCREEN" => dgettext("audio","Fullscreen"),
	);


	// First, we need to see if the given config is OK
	// ToDo


	// We build the BACK_LINK

	$_DATA["BACK_LINK"] = "../../_menus/menu.php?menu=medias";


	// And finally, the main part

	$player = @new MPD_Controller($MODULE_CONFIG["HOST"],$MODULE_CONFIG["PORT"],$MODULE_CONFIG["PASS"]);

	if($player->is_connected())
	{
		// Let's update the database :
		@$player->db_update();

		// And then, build the library :
		$artists = array();
		$albums = array();
		$_DATA["ALBUMS"] = array();

		// Getting the covers :
		$albums = $player->list_albums();

		foreach($albums as $a)
		{
			$artist = $a["artist"];
			$album = $a["album"];

			$cover_path = $Z3NB0X_CONFIG["MEDIA_PATH"]."/".$MODULE_CONFIG["ROOTDIR"]."/".$artist."/".$album."/cover.jpg";

			if(file_exists($cover_path))
				$cover_url = urlencode($cover_path);
			else
				$cover_url = urlencode($MODULE_CONFIG["DEFAULT_COVER"]);

			$_DATA["ALBUMS"][] = array(
				"ARTIST" => htmlentities(utf8_decode($artist)),
				"ALBUM" => htmlentities(utf8_decode($album)),
				"COVER" => $cover_url,
			);
		}

		$player->disconnect();
	}
	else
	{
		$_DATA["ERR"] = array(
			"ERROR" => sprintf(dgettext("audio", "No connexion on %s:%s"), $MODULE_CONFIG["HOST"], $MODULE_CONFIG["PORT"]),
			"EXPLANATION" => dgettext("audio", "I wasn't able to connect to MPD. Please check your <i>config.inc.php</i> file and ensure that MPD is running :)"),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/index.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/../../error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>