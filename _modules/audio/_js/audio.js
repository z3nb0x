
// VARIABLES -------------------------------------------------------------------

var _playing = false;
var _state = "stop";
var _pos = -1;
var _status = null;

var _artist = "";
var _album = "";
var _title = "";

var _icones_path;





// INITIALIZATION ----------------------------------------------------------------------------------

function init()
{
	_icones_path = "../../_themes/"+document.getElementById("theme").value+"/images/player/";

	init_status();

	init_play_pause();

	btn_play();
	btn_stop();
	btn_next();
	btn_back();
}

function init_play_pause()
{
	if(_playing)
		document.getElementById("img_play").src = _icones_path+"pause.png";
	else
		document.getElementById("img_play").src = _icones_path+"play.png";
}

function init_status()
{
	ajax_doXML("status.php",do_status,true);

	_status = setTimeout(init_status,1000);
}

function do_status(p)
{
	var pos = -1;
	var artist = "";
	var album = "";
	var title = "";
	var state = "";

	var state = p.getElementsByTagName("state")[0].firstChild.nodeValue;
	var pos = p.getElementsByTagName("pos")[0].firstChild.nodeValue;


	// Let's see if we need to update the info :

	if((pos != _pos) || (state != _state))
	{
	// Seems we need to update :

		_pos = pos;
		_state = state;

		if(p.getElementsByTagName("artist")[0].firstChild)
			artist = p.getElementsByTagName("artist")[0].firstChild.nodeValue;

		if(p.getElementsByTagName("album")[0].firstChild)
			album = p.getElementsByTagName("album")[0].firstChild.nodeValue;

		if(p.getElementsByTagName("title")[0].firstChild)
			title = p.getElementsByTagName("title")[0].firstChild.nodeValue;


		if(state=="play")
			_playing = true;
		else
			_playing = false;

		// Let's update the GUI :

		init_play_pause();

		if(artist != _artist)
			document.getElementById("artist").innerHTML = artist;

		if(album != _album)
			document.getElementById("album").innerHTML = album;

		if(title != _title)
			document.getElementById("title").innerHTML = title;


		// Let's update the others properties :

		if(artist != _artist)
			_artist = artist;

		if(album != _album)
			_album = album;

		if(title != _title)
			_title = title;
	}
}





// BUTTONS -----------------------------------------------------------------------------------------


function btn_play()
{
	document.getElementById("btn_play").onclick = function()
	{
		play();

		return false;
	}
}

function btn_stop()
{
	document.getElementById("btn_stop").onclick = function()
	{
		stop();

		return false;
	}
}

function btn_next()
{
	document.getElementById("btn_next").onclick = function()
	{
		next();

		return false;
	}
}


function btn_back()
{
	document.getElementById("btn_prev").onclick = function()
	{
		previous();

		return false;
	}
}





// ACTIONS -----------------------------------------------------------------------------------------


function play()
{
	request_and_stay("","player.php?cmd=play");
}


function next()
{
	request_and_stay("","player.php?cmd=next");
}


function previous()
{
	request_and_stay("","player.php?cmd=prev");
}


function stop()
{
	request_and_stay("","player.php?cmd=stop");
}


function clear_playlist()
{
	stop();
	request_and_stay("","player.php?cmd=clear");

	return false;
}

function add_to_playlist(type,val)
{
	var u = "add_to_playlist.php?";
	u += encodeURIComponent(type);
	u += "=";
	u += encodeURIComponent(val);

	request_and_stay("",u);

	return false;
}