<?php
	// REQUIRE

	require_once(dirname(__FILE__)."/../_inc/config.inc.php");
	require_once(dirname(__FILE__)."/../_inc/Browser.class.php");



	// VARS

	$b = new Browser(".");
	$b->hide_hidden_dirs();
	$ok = $b->getDirs();

	$menu = "index/menu.xml";

	if(in_array($_GET["menu"],$ok))
		$menu = $_GET["menu"]."/menu.xml";



	// TRAITEMENT

	// Chargement du fichier XML
	$xml = new DOMDocument;
	$xml->load($menu);

	// Chargement du fichier XSL
	$xsl = new DOMDocument;
	$xsl->load("menu.xsl");

	// Chargement du transformateur
	$proc = new XSLTProcessor;

	// Gestion du thème
	$proc->setParameter(null,"theme",$_Z3N__THEME);

	// Transformation
	$proc->importStylesheet($xsl);
	$html = $proc->transformToDoc($xml);



	// AFFICHAGE

	$res = $html->saveHTML();
	echo $res;
?>