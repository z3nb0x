﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

<html>
<head>
	<title>z3nb0x [0.2] - Ze ENhanced b0x !</title>
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" type="text/css" href="../_themes/z3nGrass/css/general.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../_themes/z3nGrass/css/menus.css" media="screen" />
</head>

<body>
	<h1><xsl:value-of select="/menu/title" /></h1>

	<img class="plugin_img">
		<xsl:attribute name="src">
			<xsl:value-of select="/menu/picture" />
		</xsl:attribute>
	</img>

	<div id="main">
		<div id="menu">
			<ul>
				<xsl:apply-templates select="/menu/button" />
			</ul>
		</div>
	</div>

	<xsl:apply-templates select="/menu/retour" />
</body>
</html>

</xsl:template>


<!-- TEMPLATE pour les éléments BUTTON -->

<xsl:template match="button">
	<xsl:variable name="what">
		<xsl:value-of select="type" />
	</xsl:variable>

	<xsl:variable name="href">
		<xsl:value-of select="action" />
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="$what = 'menu'">
			<xsl:call-template name="doButton">
				<xsl:with-param name="link">
					<xsl:value-of select="concat('menu.php?menu=',$href)" />
				</xsl:with-param>
				<xsl:with-param name="image">
					<xsl:value-of select="concat($href,'/menu.png')" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="doButton">
				<xsl:with-param name="link">
					<xsl:value-of select="concat('../_modules/',$href,'/index.php')" />
				</xsl:with-param>
				<xsl:with-param name="image">
					<xsl:value-of select="concat('../_modules/',$href,'/menu.png')" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="retour">
	<xsl:variable name="what">
		<xsl:value-of select="type" />
	</xsl:variable>

	<xsl:variable name="href">
		<xsl:value-of select="action" />
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="$what = 'menu'">
			<xsl:call-template name="doRetour">
				<xsl:with-param name="link">
					<xsl:value-of select="concat('menu.php?menu=',$href)" />
				</xsl:with-param>
				<xsl:with-param name="image">
					<xsl:value-of select="concat($href,'/menu.png')" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="doRetour">
				<xsl:with-param name="link">
					<xsl:value-of select="concat('../_modules/',$href,'/index.php')" />
				</xsl:with-param>
				<xsl:with-param name="image">
					<xsl:value-of select="concat('../_modules/',$href,'/menu.png')" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="doButton">
	<xsl:param name="link" select="../index/menu_index.xml" />
	<xsl:param name="image" select="../index/menu.png" />

	<li>
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="$link" />
			</xsl:attribute>

			<img>
				<xsl:attribute name="src">
					<xsl:value-of select="$image" />
				</xsl:attribute>
			</img>

			<xsl:value-of select="text" />
		</a>
	</li>
</xsl:template>


<xsl:template name="doRetour">
	<xsl:param name="link" select="../index/menu_index.xml" />

	<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$link" />
		</xsl:attribute>
		<xsl:attribute name="id">
			<xsl:value-of select="@id"/>
		</xsl:attribute>

		<xsl:value-of select="text" />
	</a>
</xsl:template>

</xsl:stylesheet>
