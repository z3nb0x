<?php
	// REQUIRE _____________________________________________________________

	require_once(dirname(__FILE__)."/_inc/config.inc.php");
	require_once(dirname(__FILE__)."/_inc/Menu.class.php");





	// i18n ________________________________________________________________

	bindtextdomain("menu",dirname(__FILE__)."/../../_i18n/");
	bind_textdomain_codeset("menu","UTF-8");





	// BUILD THE DATA ($_DATA) _____________________________________________

	$menukey = "menu_home";
	$menu = new Menu("MENU.ini");
	$m = @$menu->buildMenu($menukey);

	if($m !== false)
	{
		$_DATA = array(
			"MODULE" => "menu",
			"THEME" => $Z3NB0X_CONFIG["THEME"],
			"TITLE" => htmlentities(utf8_decode($m["title"])),
			"IMG" => htmlentities("images/".$menukey.".png"),
			"ITEMS" => array(),
		);

		foreach($m as $k=>$item)
		{
			if($k != "title")
			{
				$_DATA["ITEMS"][] = array(
					"TITLE" => dgettext("menu", $item),
					"URL" => "",
					"IMG" => "",
				);
			}
		}
	}
	else
	{
		$_DATA["ERR"] = array(
			"ERROR" => dgettext("menu", "This menu doesn't exist !"),
			"EXPLANATION" => dgettext("menu", "The provided key wasn't found in your MENU.ini file. Please check your MENU.ini file."),
		);
	}





	// DISPLAY THE PAGE ____________________________________________________

	ob_start();

	if(!isset($_DATA["ERR"]))
	{
	// Everything seems OK : no error
		include(dirname(__FILE__)."/menu.tpl.php");
	}
	else
	{
	// Doh ! An error occured
		include(dirname(__FILE__)."/error.php");
	}

	// Gets the page
        $page = ob_get_contents();
        ob_end_clean();

	// Displays the page
 	echo $page;
?>